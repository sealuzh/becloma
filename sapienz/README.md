### Environment Configration
* Python: 2.7

* Android SDK:
    API 19

* Linux:
    sudo apt-get install libfreetype6-dev libxml2-dev libxslt1-dev python-dev

* Mac OS:
    brew install coreutils for gtimeout

Install Python dependencies:

    sudo pip install -r requirements.txt


## Usage
    python main.py <apk_path | source_folder_path>

where apk\_path is path to the subject apk under test  
or you can specify source\_folder\_path for the subject app with source code

**N.b:** It would be better to place the target into the `outs` directory and run for example
    python main.py outs/com.app.nice.app.apk

### Subject Requirement:
* instrumented apk should be compiled and named with suffix "-debug.apk"
* closed-source/non-instrumented apk name should end with ".apk" 

### Settings
Set the ANDROID_HOME, the WORKING_DIR (the Sapienz home) and the name of the emulator.

### Output
* for open-sourced apps, outputs are stored under the given source folder
* for closed-sourced apps, output are stored under <apk_file_path>_output

Output content:

    /coverages - Coverage reports are stored here
    
    /crashes - Crash reports and corresponding test cases that lead to the crashes 
    (and also recorded videos files when using real devices)
    
    /intermediate - Generated test event sequences for each generation; logbook of the genetic evolution; 
    and line charts showing the variation trend for each objectives.

