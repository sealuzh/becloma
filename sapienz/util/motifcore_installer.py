# Copyright (c) 2016-present, Ke Mao. All rights reserved.


import os
import settings


def install(motifcore_path, motifcore_script_path, device):
    # obtain write permission
    # in order to make the mount work

    # Start the device as root if it is a real_device
    if settings.IS_DEVICE == "True":
        os.system("adb root")

    os.system("adb -s " + device + " shell mount -o rw,remount /system")

    # push
    os.system("adb -s " + device + " push " + motifcore_script_path + " /system/bin")

    os.system("adb -s " + device + " push " + motifcore_path + " /system/framework")

    # recover permission
    #os.system("adb -s " + device + " shell mount -o ro,remount /system")


if __name__ == "__main__":
    os.chdir("..")
    install("lib/motifcore.jar", "resources/motifcore", "emulator-5554")