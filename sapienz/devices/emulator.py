# Copyright (c) 2016-present, Ke Mao. All rights reserved.


import os
import time

import subprocess as sub

import settings
from util import motifcore_installer
from util import pack_and_deploy


def get_devices():
    """ will also get devices ready
    :return: a list of avaiable devices names, e.g., emulator-5556
    """
    ret = []
    p = sub.Popen('adb devices', stdout=sub.PIPE, stderr=sub.PIPE, shell=True)
    output, errors = p.communicate()
    print output
    segs = output.split("\n")
    for seg in segs:
        device = seg.split("\t")[0].strip()
        if seg.startswith("emulator-"):
            p = sub.Popen('adb -s ' + device + ' shell getprop init.svc.bootanim', stdout=sub.PIPE, stderr=sub.PIPE, shell=True)
            output, errors = p.communicate()
            if output.strip() != "stopped":
                time.sleep(10)
                print "waiting for the emulator:", device
                return get_devices()
            else:
                ret.append(device)

    assert len(ret) > 0

    return ret


def boot_devices():
    """
    prepare the env of the device
    add -no-audio -no-window to sub.Popen
    :return:
    """
    for i in range(0, settings.DEVICE_NUM):
        device_name = settings.AVD_SERIES
                      # + str(i)
        print "Booting Device:", device_name
        time.sleep(0.3)
        if settings.HEADLESS:
            sub.Popen('emulator -avd ' + device_name + " -wipe-data",
                      stdout=sub.PIPE, stderr=sub.PIPE, shell=True)
        else:
            sub.Popen('emulator -avd ' + device_name + " -wipe-data",
                      stdout=sub.PIPE, stderr=sub.PIPE, shell=True)


def clean_sdcard():
    for device in get_devices():
        os.system("adb -s " + device + " shell mount -o rw,remount rootfs /")
        os.system("adb -s " + device + " shell chmod 777 /mnt/sdcard")

        os.system("adb -s " + device + " shell rm -rf /mnt/sdcard/*")


def prepare_motifcore():
    for device in get_devices():
        motifcore_installer.install(settings.WORKING_DIR + "lib/motifcore.jar", settings.WORKING_DIR + "resources/motifcore", device)


def pack_and_deploy_aut():
    # instrument the app under test
    pack_and_deploy.main(get_devices())


def destory_devices():
    # for device in get_devices():
    # 	os.system("adb -s " + device + " emu kill")
    # do force kill
    os.system("kill -9  $(ps aux | grep 'emulator' | awk '{print $2}')")


if __name__ == "__main__":
    destory_devices()