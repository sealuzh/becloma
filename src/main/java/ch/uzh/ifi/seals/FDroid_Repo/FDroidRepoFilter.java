package ch.uzh.ifi.seals.FDroid_Repo;


import ch.uzh.ifi.seals.Parsers.DatasetParser;
import ch.uzh.ifi.seals.Parsers.SAXParserXML;
import ch.uzh.ifi.seals.costants.Enumerations.Extensions;
import ch.uzh.ifi.seals.costants.Links;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

/**
 * The FDroidRepoFilter class filters the FDroid Repo XML file and extracts from it some APK/SRCS links, which are
 * going to be downloaded
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 23.12.2017
 */
public class FDroidRepoFilter {

    private SAXParserXML saxParserXML;
    private HashMap<String, Map<String, Date>> apksPerAppCollector;
    private ArrayList<String> fDroidApksLinks;
    private HashMap<String, Date> APKSAndDatesFromXMLFile;
    private HashMap<String, ArrayList<Date>> dataset;

    public FDroidRepoFilter() throws IOException {
        this.saxParserXML = new SAXParserXML();
        this.apksPerAppCollector = new HashMap<>();
        this.fDroidApksLinks = saxParserXML.getAPKSLinksFromXMLFile();
        this.APKSAndDatesFromXMLFile = saxParserXML.getAPKSWithDateFromXMLFile();
        this.dataset = DatasetParser.getPackagesWithDatesFromCSV();
    }

    /**
     * The stream filters the APK-Links from the XML File. After filtering, it contains the name of all packages and
     * their apk version, which are available on the FDroid XML file.
     * With this method the stream gets initialized
     *
     * @return (stream of String[] containing at the first position the name of the package, and the second the APK version)
     */
    private Stream<String[]> getStream() {
        return this.fDroidApksLinks.stream()
                .filter(link -> link.contains("_") && link.contains(".apk"))
                .map(link -> link.substring(Links.API_FDROID.length()).split("_"));
    }

    /**
     * The stream filters the APK-Links from the XML File. It compares the provided dataset with the Fdroid XML File.
     * After filtering it contains a list of the common packages (stored as APK links) between the XML file and the dataset.
     *
     * @return (stream of String containing a list of filtered apk links)
     */
    private Stream<String> getStreamForAPKSAndDates() {
        return this.APKSAndDatesFromXMLFile.entrySet().stream()
                .map(Map.Entry::getKey)
                .filter(app -> dataset.keySet().contains(app.substring(Links.API_FDROID.length()).split("_")[0]));
    }

    /**
     * @param CSV_DATASET (Dataset containing a list of apps)
     * @return
     * @throws IOException
     */
    public ArrayList<String> getLinksLastAPKVersion(final ArrayList<String> CSV_DATASET) throws IOException {
        return filterFDroidWithDataSet(CSV_DATASET, Extensions.ANDROID_APP.prefix());
    }

    public ArrayList<String> getLinksLastSRC(final ArrayList<String> CSV_DATASET) throws IOException {
        return filterFDroidWithDataSet(CSV_DATASET, Extensions.SOURCE.prefix() + Extensions.ZIP.prefix());
    }

    /**
     * It compares the data_set provided in the <config.properties> file with the FDroidRepo XML file and returns
     * an ArrayList of filtered data (based on the extension file)
     *
     * @param CSV_DATASET    (given dataset)
     * @param file_extension (given file extension)
     * @return (An ArrayList of filtered strings containing links for downloading data from the FDroid repo)
     */
    private ArrayList<String> filterFDroidWithDataSet(ArrayList<String> CSV_DATASET, String file_extension) {
        ArrayList<String> filteredList = new ArrayList<>();
        CSV_DATASET.forEach(csvApp -> {
            final boolean[] isLastVersion = {true};
            getStream().forEach(x -> {
                String fDroidApp = x[0];
                if (fDroidApp.equals(csvApp) && isLastVersion[0]) {
                    isLastVersion[0] = false;
                    int apkVersion = Integer.valueOf(x[1].split("\\.")[0]);
                    filteredList.add(Links.API_FDROID + fDroidApp + "_" + apkVersion + file_extension);
                }
            });
        });
        return filteredList;
    }


    /**
     * It compares the dataset (DatasetParser.getPackagesWithDatesFromCSV()) with the FDroid Repo and returns
     * an ArrayList of string containing links from the FDroid Repo.
     * For each APK it takes the one which has been added and published on FDroid with the closest date to the
     * start_review in the Dataset.
     *
     * @return A list of FDroid links
     */
    public ArrayList<String> getFilteredFDroidRepoWithAPKSAndDates() throws InterruptedException {
        ArrayList<String> apksToDownload = new ArrayList<>();
        initializeApksPerAppCollector();

        getStreamForAPKSAndDates().forEach(apkLink -> {
            String packageName = apkLink.substring(Links.API_FDROID.length()).split("_")[0];
            Map<String, Date> treeMap = this.apksPerAppCollector.get(packageName); // treeMap gets initialized
            treeMap.put(apkLink, this.APKSAndDatesFromXMLFile.get(apkLink));// APK_LINK with the corresponding data id added into the treemap
            this.apksPerAppCollector.put(packageName, treeMap); // the key(packageName) gets updated with the just here above updated treemap
        });

        this.dataset.forEach((packageName, dates) -> {
            Date startDateReview = dates.get(0);
            Date chosenDate = new Date();
            if (this.apksPerAppCollector.containsKey(packageName)) {
                Map<String, Date> treeMap = this.apksPerAppCollector.get(packageName);
                try {
                    chosenDate = getNearestDate(new ArrayList<>(treeMap.values()), startDateReview);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Date finalChosenDate = chosenDate;
                treeMap.forEach((apkLink, date) -> {
                    if (finalChosenDate.equals(date)) {
                        apksToDownload.add(apkLink);
                    }
                });
            }
        });
        return apksToDownload;
    }

    /**
     * initialises the ApksPerAppCollector, which is a kind of database that contains for each packagename a list of
     * apks associated with their addition date on FDroid.
     */
    private void initializeApksPerAppCollector() {
        getStreamForAPKSAndDates().forEach(apkLink -> {
                    String packageName = apkLink.substring(Links.API_FDROID.length()).split("_")[0];
                    this.apksPerAppCollector.put(packageName, new TreeMap<>());
                }
        );
    }

    /**
     * It compares a set of dates with a target date and returns the closest date in the list to the target date
     *
     * @param dates      (list of dates)
     * @param targetDate (targetDate)
     * @return (closest date to the target date)
     * @throws InterruptedException
     */
    private Date getNearestDate(ArrayList<Date> dates, Date targetDate) throws InterruptedException {
        DateTime targetJodaDate = new DateTime(targetDate);
        DateTime returnDate = new DateTime(targetDate);
        DateTime jodaDate;
        ArrayList<Integer> toSort = new ArrayList<>();
        for (Date date : dates) {
            jodaDate = new DateTime(date);
            Days d = Days.daysBetween(jodaDate, targetJodaDate);
            toSort.add(Math.abs(d.getDays()));
        }
        int min = Collections.min(toSort);
        for (Date date : dates) {
            jodaDate = new DateTime(date);
            Days d = Days.daysBetween(jodaDate, targetJodaDate);
            if (Math.abs(d.getDays()) == min) {
                returnDate = jodaDate;
                break;
            }
        }
        return returnDate.toDate();
    }

    /**
     * Simply prints the collector
     */
    public void printapksPerAppCollector() {
        this.apksPerAppCollector.forEach((key, value) -> {
            System.out.println("  Packagename:   " + key);
            for (String apkLink : value.keySet()) {
                System.out.print("\t\tAPK LINK: " + apkLink);
                System.out.println("\t\tDATE:  " + value.get(apkLink));
            }
        });
    }
}
