package ch.uzh.ifi.seals.config.Exceptions;

/**
 * This class extends the Exception class and handles the exceptions when the emulator on which you want to test
 * the dataset is not installed .
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 05.04.2017
 */
public class EmulatorExceptions extends Exception{

    public EmulatorExceptions(String message) {
        super(message);
    }

    public EmulatorExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
