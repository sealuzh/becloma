package ch.uzh.ifi.seals.config.Exceptions;

/**
 * This class extends the Exception class and handles the exceptions when a String cannot be converted into a number.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 05.04.2017
 */
public class NumberExceptions extends Exception {

    public NumberExceptions(String message) {
        super(message);
    }

    public NumberExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
