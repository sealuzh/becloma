package ch.uzh.ifi.seals.config.Exceptions;

/**
 * This class extends the Exception class and handles the exceptions when a String cannot be converted into a boolean flag.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 05.04.2017
 */
public class BooleanExceptions extends Exception {


    public BooleanExceptions(String message, Throwable cause) {
        super(message, cause);
    }

    public BooleanExceptions(String message) {
        super(message);
    }
}
