package ch.uzh.ifi.seals.config.Exceptions;

/**
 * This class extends the Exception class and handles the exceptions when a directory inserted in the Configuration File
 * and used inside the project doesn't exists.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 05.04.2017
 */
public class DirectoryExceptions extends Exception {

    public DirectoryExceptions(String message, Throwable cause) {
        super(message, cause);
    }

    public DirectoryExceptions(String message) {
        super(message);
    }
}
