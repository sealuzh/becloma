package ch.uzh.ifi.seals.config.Exceptions;

/**
 * Created by LuckyP on 12.04.17.
 */
public class ArgsExceptions extends Exception{

    public ArgsExceptions(String message) {
        super(message);
    }

    public ArgsExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
