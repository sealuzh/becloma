package ch.uzh.ifi.seals.config;

import ch.uzh.ifi.seals.costants.Enumerations.Extensions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * This class represents the connection between the project and the <config.properties> file
 *
 * @author Giovanni Grano
 * @version 1.0
 * @since 24.01.2017
 */
public class ConfigurationManager {

    private static final String STATIC_CONTENT = "---------STATIC+CONTENT";
    private static ConfigurationManager instance;
    private static String filename;
    private static String content;

    static {
        setFilename("config" + Extensions.CONFIG.prefix());
    }

    private String myFilename;
    private Properties properties;

    private ConfigurationManager(String filename) throws IOException {
        this.properties = new Properties();
        this.properties.load(new FileInputStream(filename));
        this.myFilename = filename;
    }

    private ConfigurationManager() {
        this.myFilename = "";
    }

    /**
     * Sets the filename of the configuration file. To be called before <code>getInstance</code>.
     *
     * @return
     */
    public static void setFilename(String pFilename) {
        filename = pFilename;
        content = null;
    }

    public static void setDirectContent(String pContent) {
        content = pContent;
        filename = STATIC_CONTENT + UUID.randomUUID();
    }

    /**
     * Returns an instance of the configuration manager. Call <code>setFilename</code> before this method.
     *
     * @return the instance of <ConfigurationManager>
     */
    public static ConfigurationManager getInstance() {
//        instance = new ConfigurationManager();
        try {
            if (instance == null || !instance.myFilename.equals(filename)) {
                if (!filename.startsWith(STATIC_CONTENT))
                    instance = new ConfigurationManager(filename);
                else {
                    instance = new ConfigurationManager();
                    instance.directLoadContent(filename, content);
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
            System.err.println("A problem occurred with the config Manager");
        }
        return instance;
    }

    public Method[] getMethods() {
        return this.getClass().getMethods();
    }

    private void directLoadContent(String pFilename, String pContent) throws IOException {
        this.properties = new Properties();
        this.properties.load(new StringReader(pContent));
        this.myFilename = pFilename;
    }

    public String getMonkeyReportsDirectory() {
        return this.properties.getProperty("monkey_reports_directory");
    }

    public String getSapienzReportsDirectory() {
        return this.properties.getProperty("sapienz_reports_directory");
    }

    public String getGradleFilesDirectory() {
        return this.properties.getProperty("all_gradle_files_directory");
    }

    public String getFDroidRepoXmlDirectory() {
        return this.properties.getProperty("fdroid_repo_xml_directory");
    }

    public String getCSVDirectory() {
        return this.properties.getProperty("csv_directory");
    }

    public String getReviewDirectory() {
        return this.properties.getProperty("review_directory");
    }
    public String getSaveApkDirectory() {
        return this.properties.getProperty("save_apk_directory");
    }

    public String getSaveSrcDirectory() {
        return this.properties.getProperty("save_src_directory");
    }

    public String getAdbExecDirectory() {
        return this.properties.getProperty("adb_exec_directory");
    }

    public String getEmulatorName() {
        return this.properties.getProperty("emulator_name");
    }

    public String getDeviceName() {
        return this.properties.getProperty("device_name");
    }

    public String getSapienzWorkDirectory() {
        return this.properties.getProperty("sapienz_work_directory");
    }

    public String getSurfDirectory() {
        return this.properties.getProperty("surf_directory");
    }

    public String getToolDirectory() {
        return this.properties.getProperty("sapienz_tool_directory");
    }

    public String getCrashContainerDirectory() {
        return this.properties.getProperty("crash_log_container");
    }

    public String getCrashContainerIndexDirectory() {
        return this.properties.getProperty("crash_log_index_directory");
    }

    public String minutesPerApp() {
        return this.properties.getProperty("minutes_per_app");
    }


    /**
     * Timeouts
     */
    public TimeUnit TimeoutUnit() {
        if (this.properties.getProperty("TIME_UNIT").equalsIgnoreCase("sec")) {
            return TimeUnit.SECONDS;
        } else if (this.properties.getProperty("TIME_UNIT").equalsIgnoreCase("min")) {
            return TimeUnit.MINUTES;
        }
        System.err.println("GIVEN TIME_UNIT IN THE CONFIG FILE IS NOT CORRECT! MINUTES ARE SET AS DEFAULT");
        return TimeUnit.MINUTES;
    }

    public Long monkeyTimeout() {
        return Long.valueOf(this.properties.getProperty("MONKEY_TIMEOUT"));
    }

    public Long sapienzTimeout() {
        return Long.valueOf(this.properties.getProperty("SAPIENZ_TIMEOUT"));
    }

    /**
     * Monkey testing parameters
     */
    public String getVerbosity() {
        return this.properties.getProperty("verbosity", "-v");
    }

    public String getPackageAllowed() {
        return this.properties.getProperty("package_allowed");
    }

    public String getNoRandomEvents() {
        return this.properties.getProperty("random_events", "100");
    }

    public String getPercentageTouchEvent() {
        return this.properties.getProperty("percentage_touch_events", "0");
    }

    public String getPercentageMotionEvent() {
        return this.properties.getProperty("percentage_motion_events", "0");
    }

    public String getPercentageSystemEvent() {
        return this.properties.getProperty("percentage_system_events", "0");
    }

    public String getIgnoreCrashes() {
        return this.properties.getProperty("ignore_crashes", "False");
    }

    public String getDelayBetweenEvents() {
        return this.properties.getProperty("delay_between_events", "0");
    }

    /**
     * Sapienz testing parameters
     *
     * @return
     */

    public String getAVDBootDelay() {
        return this.properties.getProperty("AVD_BOOT_DELAY", "30");
    }

    public String getSequenceLengthMin() {
        return this.properties.getProperty("SEQUENCE_LENGTH_MIN", "20");
    }

    public String getSequenceLengthMax() {
        return this.properties.getProperty("SEQUENCE_LENGTH_MAX", "500");
    }

    public String getSuiteSize() {
        return this.properties.getProperty("SUITE_SIZE", "5");
    }

    public String getPopulationSize() {
        return this.properties.getProperty("POPULATION_SIZE", "50");
    }

    public String getOffspringSize() {
        return this.properties.getProperty("OFFSPRING_SIZE", "50");
    }

    public String getGeneration() {
        return this.properties.getProperty("GENERATION", "100");
    }

    public String getCrossoverProbability() {
        return this.properties.getProperty("CXPB", "0.7");
    }

    public String getMutationProbability() {
        return this.properties.getProperty("MUTPB", "0.3");
    }

    public String getNumberOfIterations() {
        return this.properties.getProperty("nr_of_iterations", "1");
    }
}