package ch.uzh.ifi.seals.config;


import ch.uzh.ifi.seals.Main.Main;
import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.Exceptions.*;
import ch.uzh.ifi.seals.costants.Enumerations.Extensions;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ch.uzh.ifi.seals.Main.Main.*;

/**
 * The ConfigurationManagerValidator class checks whether the <config.properties> file is well-written and
 * has valid / allowed parameters
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 04.04.2017
 */
public class ConfigurationManagerValidator {


    /**
     * It validates the whole <config.properties> file. It goes through all the methods declared in the ConfigurationManager Class
     * and with some regex checks the validity of parameters in the config File (<config.properties>).
     *
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws BooleanExceptions
     * @throws DirectoryExceptions
     * @throws EmulatorExceptions
     * @throws NumberExceptions
     * @throws DeviceExceptions
     */
    public static void validateConfigurationManager() throws Exception {
        Method[] methods = ConfigurationManager.getInstance().getMethods();
        ArrayList<Regex> regex = getPatterns();
        for (Method method : methods) {
            for (Regex re : regex) {
                Matcher m = Pattern.compile(re.getRegex()).matcher(method.getName().trim());
                if (m.find()) {
                    switch (re) {
                        case BOOLEAN:
                            validateBoolean(method);
                            break;
                        case DIRECTORIES:
                            validateDirectory(method);
                            break;
                        case EMULATOR_NAME:
                            validateEmulatorName(method);
                            break;
                        case NUMBERS:
                            validateNumber(method);
                            break;
                        case DEVICE_NAME:
                            validateDeviceName(method);
                            break;
                    }
                }
            }

        }

    }

    /**
     * Validates the Device Name. It checks whether the device can be used for starting a testing session
     *
     * @param method (The given Method which has to be investigated)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws DirectoryExceptions
     * @throws DeviceExceptions
     */
    private static void validateDeviceName(Method method) throws InvocationTargetException, IllegalAccessException, DirectoryExceptions, DeviceExceptions {
        final String DEVICE_NAME = ((String) method.invoke(ConfigurationManager.getInstance())).trim();
            if (IS_DEVICE) { // if the tests are computed on a real device
                if (!isInTheDevicesList(DEVICE_NAME)) {
                    throw new DeviceExceptions(DEVICE_NAME + " is not attached in the list of all the available devices");
                }
        }
    }

    /**
     * Returns a boolean value. True if the device is in the list of all available devices; Otherwise false.
     *
     * @param DEVICE_NAME (Name of the device provided in the config File)
     * @return (boolean value)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws DirectoryExceptions
     */
    private static boolean isInTheDevicesList(String DEVICE_NAME) throws InvocationTargetException, IllegalAccessException, DirectoryExceptions {
        CommandExecutor command = new CommandExecutor();
        validateDirectory(ConfigurationManager.getInstance().getAdbExecDirectory());
        String attached_devices = command.getOutputFromTerminal(ConfigurationManager.getInstance().getAdbExecDirectory() + " devices");
        return attached_devices.contains(DEVICE_NAME);
    }


    /**
     * It validated if the String can be converted into a Number. Otherwise an exception gets thrown.
     *
     * @param method (The given Method which has to be investigated)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NumberExceptions
     */
    private static void validateNumber(Method method) throws InvocationTargetException, IllegalAccessException, NumberExceptions {
        final String NUMBER = ((String) method.invoke(ConfigurationManager.getInstance())).trim();
        if (!areAllDigits(NUMBER)) {
            throw new NumberExceptions(method.getName() + "() in the class " + method.getDeclaringClass() + " has not a valid Number format! Please check it in" +
                    "the config File.", new NumberFormatException());
        }
    }

    /**
     * It checks if the given String can be properly converted into a Number. It goes through all chars of the
     * given String NUMBER and verifies that each char is a digit
     *
     * @param NUMBER (The given String which has to be investigated)
     *               '     * @return (true or false)
     */
    private static boolean areAllDigits(String NUMBER) {
        for (int _char = 0; _char < NUMBER.length(); _char++) {
            if (NUMBER.charAt(_char) == '.') {
                continue;
            }
            if (!Character.isDigit(NUMBER.charAt(_char))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validates whether the emulator exists and is properly installed and can be used for a testing session
     *
     * @param method (Method which has to be investigated)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws DirectoryExceptions
     * @throws EmulatorExceptions
     */
    private static void validateEmulatorName(Method method) throws InvocationTargetException, IllegalAccessException, DirectoryExceptions, EmulatorExceptions {
        String temp[] = ConfigurationManager.getInstance().getAdbExecDirectory().split("/");
        final String EMULATOR_NAME = ((String) method.invoke(ConfigurationManager.getInstance()));
        final String AVD_DIRECTORY = "/" + temp[1] + "/" + temp[2] + "/.android/avd";
        validateDirectory(AVD_DIRECTORY);
        if (!isInTheEmulatorsList(AVD_DIRECTORY, EMULATOR_NAME)) {
            throw new EmulatorExceptions(EMULATOR_NAME + " is not in the list of the available emulators! Please check that it is installed or that it has a valid name", new IllegalArgumentException());
        }
    }

    /**
     * returns a boolean value. True if the EMULATOR is the list of the available emulators, False otherwise
     *
     * @param AVD_DIR       (The Android directory where all the emulators are stored)
     * @param EMULATOR_NAME (The name of the emulator provided in the config File)
     * @return (true or false)
     */
    private static boolean isInTheEmulatorsList(final String AVD_DIR, final String EMULATOR_NAME) {
        File[] avd_files = new File(AVD_DIR).listFiles();
        ArrayList<String> avd_names = new ArrayList<>();
        assert avd_files != null;
        for (File avd_file : avd_files) {
            if (avd_file.isDirectory() && avd_file.getName().endsWith(Extensions.EMULATOR.prefix())) {
                String avd_name = avd_file.getName().replace(Extensions.EMULATOR.prefix(), "");
                avd_names.add(avd_name);
            }
        }
        return avd_names.contains(EMULATOR_NAME);
    }

    /**
     * Validates a boolean variable. If the variable is not a valid boolean value, an exception gets thrown
     *
     * @param method (method which has to be investigated)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws BooleanExceptions
     */
    private static void validateBoolean(Method method) throws InvocationTargetException, IllegalAccessException, BooleanExceptions {
        final String BOOLEAN_STRING = ((String) method.invoke(ConfigurationManager.getInstance())).toLowerCase().trim();
        if (!isAValidBoolean(BOOLEAN_STRING)) {
            throw new BooleanExceptions(method.getName() + "() in the class " + method.getDeclaringClass() + " has not a valid boolean value or might be null. Maybe a typo?", new IllegalArgumentException());
        }

    }

    /**
     * Returns a boolean value; True is the String can be properly converted into a boolean, otherwise false.
     *
     * @param BOOLEAN_STRING (String that has to be investigated)
     * @return (True or False)
     */
    private static boolean isAValidBoolean(String BOOLEAN_STRING) {
        return BOOLEAN_STRING != null && (BOOLEAN_STRING.equalsIgnoreCase("true") || BOOLEAN_STRING.equalsIgnoreCase("false"));
    }

    /**
     * Validates the directory. It checks if the directory really exists, otherwise an exception gets thrown.
     *
     * @param method (Given method)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws DirectoryExceptions
     */
    private static void validateDirectory(Method method) throws InvocationTargetException, IllegalAccessException, DirectoryExceptions {
        final String DIRECTORY_PATH = ((String) method.invoke(ConfigurationManager.getInstance()));
        if (!new File(DIRECTORY_PATH).exists()) {
            throw new DirectoryExceptions(DIRECTORY_PATH + " does not exist! Please check the property in the Config File. Maybe a \"/\" is missing at the begininng of the Path", new FileNotFoundException());
        }
    }

    /**
     * Validates the directory. It checks if the directory really exists, otherwise an exception gets thrown.
     *
     * @param PATH (Directory's path)
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws DirectoryExceptions
     */
    private static void validateDirectory(final String PATH) throws InvocationTargetException, IllegalAccessException, DirectoryExceptions {
        if (!new File(PATH).exists()) {
            throw new DirectoryExceptions(PATH + " does not exist! Please check the property in the Config File. Maybe a \"/\" is missing at the begininng of the Path", new FileNotFoundException());
        }
    }

    /**
     * Sets an arrayList of Regex
     *
     * @return
     */
    private static ArrayList<Regex> getPatterns() {
        ArrayList<Regex> regex = new ArrayList<>();
        regex.add(Regex.BOOLEAN);
        regex.add(Regex.DIRECTORIES);
        regex.add(Regex.EMULATOR_NAME);
        regex.add(Regex.NUMBERS);
        regex.add(Regex.DEVICE_NAME);
        return regex;
    }
}
