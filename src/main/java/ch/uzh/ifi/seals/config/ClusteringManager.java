package ch.uzh.ifi.seals.config;

import ch.uzh.ifi.seals.costants.Enumerations.Extensions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by LuckyP on 15.05.17.
 */
public class ClusteringManager {

    private static final String STATIC_CONTENT = "---------STATIC+CONTENT";
    private static ClusteringManager instance;
    private static String filename;
    private static String content;

    static {
        setFilename("clustering" + Extensions.CONFIG.prefix());
    }

    private String myFilename;
    private Properties properties;

    private ClusteringManager(String filename) throws IOException {
        this.properties = new Properties();
        this.properties.load(new FileInputStream(filename));
        this.myFilename = filename;
    }

    private ClusteringManager() {
        this.myFilename = "";
    }

    /**
     * Sets the filename of the configuration file. To be called before <code>getInstance</code>.
     *
     * @return
     */
    public static void setFilename(String pFilename) {
        filename = pFilename;
        content = null;
    }

    public static void setDirectContent(String pContent) {
        content = pContent;
        filename = STATIC_CONTENT + UUID.randomUUID();
    }

    /**
     * Returns an instance of the configuration manager. Call <code>setFilename</code> before this method.
     *
     * @return the instance of <ConfigurationManager>
     */
    public static ClusteringManager getInstance() {
//        instance = new ConfigurationManager();
        try {
            if (instance == null || !instance.myFilename.equals(filename)) {
                if (!filename.startsWith(STATIC_CONTENT))
                    instance = new ClusteringManager(filename);
                else {
                    instance = new ClusteringManager();
                    instance.directLoadContent(filename, content);
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
            System.err.println("A problem occurred with the config Manager");
        }
        return instance;
    }


    private void directLoadContent(String pFilename, String pContent) throws IOException {
        this.properties = new Properties();
        this.properties.load(new StringReader(pContent));
        this.myFilename = pFilename;
    }

    public String getSimilarityTolerance() {
        return this.properties.getProperty("SIMILARITY_TOLERANCE");
    }

    public String getStackTraceDepth() {
        return this.properties.getProperty("STACK_TRACE_DEPTH");
    }

    public String getPreProcessedStackTracesDirectory() {
        return this.properties.getProperty("PRE_PROCESSED_STACK_TRACE");
    }

    public String getPackageName() {
        return this.properties.getProperty("PACKAGE_NAME");
    }
}
