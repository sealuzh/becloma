package ch.uzh.ifi.seals.config;

/**
 * The enumeration Regex creates the Regular expressions which are used for distinguishing and classifying the
 * different methods on the class Configuration Manager. Once the differentiation is completed, the <config.properties>
 * file can be validated.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 04.04.2017
 */
public enum Regex {

    BOOLEAN("(^(is|are|has|have)[A-Z].*|IgnoreCrashes)"),
    DIRECTORIES("[a-z]Directory"),
    EMULATOR_NAME("EmulatorName"),
    DEVICE_NAME("DeviceName"),
    NUMBERS("Delay|Number|minutes|Percentage|Sequence|Suite|Probability|Size|Event|Generation");


    private String regex;

    Regex(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return this.regex;
    }

}
