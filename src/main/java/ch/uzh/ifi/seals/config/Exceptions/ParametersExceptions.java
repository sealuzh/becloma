package ch.uzh.ifi.seals.config.Exceptions;

/**
 * This class extends the Exception class and handles the exceptions when the inserted parameters in the <config.proprieties>
 * file are not valid.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 08.04.2017
 */
public class ParametersExceptions extends Exception {

    public ParametersExceptions(String message) {
        super(message);
    }

    public ParametersExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
