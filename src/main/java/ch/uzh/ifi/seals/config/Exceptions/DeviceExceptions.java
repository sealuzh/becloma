package ch.uzh.ifi.seals.config.Exceptions;

/**
 * This class extends the Exception class and handles the exceptions when the device on which you want to test
 * the dataset is not attached in the list of all the available devices.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 05.04.2017
 */
public class DeviceExceptions extends Exception{

    public DeviceExceptions(String message) {
        super(message);
    }

    public DeviceExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
