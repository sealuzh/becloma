package ch.uzh.ifi.seals.Devices;

import ch.uzh.ifi.seals.Tester.CommandExecutor;

import java.io.File;

/**
 * Created by LuckyP on 13.04.17.
 */
public class Multithreading implements Runnable {

    private Thread thread;
    private String commandLine;
    private String destinationPath;
    private String deviceName;

    public Multithreading(String commandLine, File monkeyLogFile, String deviceName) {
        this.deviceName = deviceName;
        this.commandLine = commandLine;
        this.destinationPath = monkeyLogFile.getAbsolutePath();
    }

    @Override
    public void run() {
        System.out.println("Running the cmd: " + commandLine);
        try {
            CommandExecutor.generateReport(commandLine, destinationPath, deviceName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Thread for cmd: " + commandLine + " exiting.");
    }

    public Thread start() {
        System.out.println("Starting thread for cmd: " + commandLine);
        if (thread == null) {
            thread = new Thread(this, commandLine);
            thread.start();
        }
        return this.thread;
    }

}
