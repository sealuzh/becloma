package ch.uzh.ifi.seals.Devices;

import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.costants.CommandLines;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by LuckyP on 12.04.17.
 */
public abstract class Tablet {

    /**
     * Get also devices ready
     *
     * @return A list of attached devices
     * @throws IOException
     * @throws InterruptedException
     */
    public static ArrayList<String> getAllAttachedDevices() throws IOException, InterruptedException {
        ArrayList<String> attached_devices = new ArrayList<>();
        String TERMINAL_OUTPUT;

        while (true) {
            TERMINAL_OUTPUT = CommandExecutor.getOutputFromTerminal(CommandLines.ADB_DEVICES);
            if (!TERMINAL_OUTPUT.contains("* daemon not running")) {
                break;
            }
        }

        ArrayList<String> terminalOutputList = new ArrayList<>(Arrays.asList(TERMINAL_OUTPUT.split("\n")));
        Thread.sleep(200);
        for (String line : terminalOutputList) {
            if (!line.contains("List of devices attached")) {
                String device = line.split("\\s")[0].trim();
                attached_devices.add(device);
            }
        }
        return attached_devices;
    }

    public static void rebootDevices() throws IOException, InterruptedException {
        for (String device : getAllAttachedDevices()) {
            Thread t = new Thread(() -> CommandExecutor.executeCommand("adb -s " + device + " reboot"));
            t.start();
        }
    }

    public static void rootDevices() throws IOException, InterruptedException {
        for (String device : getAllAttachedDevices()) {
            Thread t = new Thread(() -> CommandExecutor.executeCommand("adb -s " + device + " root"));
            t.start();
        }

    }

    public static void getDevicesReady() {
        CommandExecutor.executeCommand("killall adb");
    }
}
