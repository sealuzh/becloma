package ch.uzh.ifi.seals.Devices;


import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.costants.CommandLines;
import ch.uzh.ifi.seals.costants.Enumerations.Extensions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static ch.uzh.ifi.seals.Main.Main.AVD_BOOT_DELAY;

/**
 * This class manages the whole Emulator environment. All the methods involved with the emulator are
 * defined in this class.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 20.12.2016
 */
public abstract class Emulator {

    private static final String EMULATOR_PACKAGES_DESTINATION_PATH = "emulator_packages" + Extensions.TEXT.prefix();
    private static ArrayList<String> installedPackagesOnTheEmulator;
    private static ConfigurationManager config;
    private static String lineIterator;
    private static Logger logger;
    private static StringBuilder stringBuilder;
    private static CommandExecutor commandExecutor;

    public Emulator() {
        logger = Logger.getLogger(getClass().getName());
        setEnvironment();
    }

    /**
     * It sets the environment for this class.
     * This method is invoked each time a new object of this class is created and instantiated.
     */
    private static void setEnvironment() {
        config = ConfigurationManager.getInstance();
        installedPackagesOnTheEmulator = new ArrayList<>();
        stringBuilder = new StringBuilder();
        commandExecutor = new CommandExecutor();
    }

    /**
     * Returns a list of all the installed packages on the emulator. In this list there are not
     * android packages installed by default.
     *
     * @return a list of installed packages
     */
    public static ArrayList<String> getInstalledPackages() throws IOException {
        Process p = CommandExecutor.getProcess(CommandLines.ADB_SHELL + " " + CommandLines.PACKAGES_LIST);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((lineIterator = reader.readLine()) != null) {
            String lineToAdd = lineIterator.substring(8);
            if (!lineToAdd.startsWith("android")) {
                installedPackagesOnTheEmulator.add(lineToAdd);
            }
        }
        return installedPackagesOnTheEmulator;
    }

    private static void startEmulator() throws IOException, InterruptedException {
        CommandExecutor.createProcess(CommandLines.EMULATOR_SHELL + " " + CommandLines.getEmulatorName());
        logger.info("Waiting " + AVD_BOOT_DELAY + " seconds for starting " + config.getEmulatorName() + "...");
        Thread.sleep(TimeUnit.SECONDS.toMillis(AVD_BOOT_DELAY));
    }

    /**
     * Starts the emulator if it is not running yet
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public static void runEmulator() throws IOException, InterruptedException {
        if (!isEmulatorRunning()) {
            startEmulator();
        }
    }

    /**
     * Returns a boolean value. True is the emulator is already running, false otherwise.
     *
     * @return (true or false)
     */
    private static boolean isEmulatorRunning() throws IOException {
        Process isRunning = CommandExecutor.getProcess(CommandLines.DEVICE_STATE);
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(isRunning.getErrorStream()));
        while ((lineIterator = errorReader.readLine()) != null) {
            stringBuilder.append(lineIterator);
        }
        String terminalOutput = stringBuilder.toString();
        return !terminalOutput.equalsIgnoreCase(CommandLines.NO_DEVICES_FOUND_MESSAGE.trim());
    }


    /**
     * It writes into the @EMULATOR_PACKAGES_DESTINATION_FILE a list of all the installed packages on the emulator
     *
     * @throws IOException
     */
    public static void writeInstalledPackages() throws IOException {
        commandExecutor.toWriteArrayList(getInstalledPackages(), EMULATOR_PACKAGES_DESTINATION_PATH);
    }
}
