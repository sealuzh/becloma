package ch.uzh.ifi.seals.Main;

/**
 * The Class Chronometer implements a chronometer for have a better overview during a testing session.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 16.03.2017
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Chronometer {
    private JFrame frame;
    private JLabel labelTime;
    private JLabel nrAppLabel;
    private JLabel itLabel;
    private JLabel appNameLabel;
    private JPanel panelButtons;
    private JPanel topPanel;
    private JPanel panelLabels;
    private JButton buttonStart;
    private JButton buttonStop;
    private Timer timer;
    private long startTime;

    public Chronometer() {
    }

    /**
     * The chronometer gets started automatically when this method is invoked
     */
    public void startChronometer() {
        defineDesign();

        timer = new Timer(50, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                long diffTime = System.currentTimeMillis() - startTime;

                int decSeconds = (int) (diffTime % 1000 / 100);
                int seconds = (int) (diffTime / 1000 % 60);
                int minutes = (int) (diffTime / 60000 % 60);
                int hours = (int) (diffTime / 3600000);

                String s = String.format("%d:%02d:%02d.%d", hours, minutes,
                        seconds, decSeconds);
                labelTime.setText(s);
            }
        });


        buttonStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startTime = System.currentTimeMillis();
                timer.start();
                buttonStart.setEnabled(false);
                buttonStop.setEnabled(true);

            }
        });

        buttonStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timer.stop();
                buttonStart.setEnabled(true);
                buttonStop.setEnabled(false);
            }
        });

        // starts auto
        buttonStart.doClick();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Defines the design for the chronometer
     */
    private void defineDesign() {
        frame = new JFrame("Chronometer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 190);

        labelTime = new JLabel("0:00:00.0");
        labelTime.setFont(new Font("SansSerif", Font.BOLD, 30));
        labelTime.setHorizontalAlignment(JLabel.CENTER);

        nrAppLabel = new JLabel("Application number:");
        nrAppLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
        nrAppLabel.setHorizontalAlignment(JLabel.CENTER);

        itLabel = new JLabel("Iteration number:");
        itLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
        itLabel.setHorizontalAlignment(JLabel.CENTER);

        appNameLabel = new JLabel("Application name ");
        appNameLabel.setFont(new Font("SansSerif", Font.BOLD, 12));
        appNameLabel.setHorizontalAlignment(JLabel.CENTER);

        buttonStart = new JButton("START");
        buttonStop = new JButton("STOP");
        buttonStop.setEnabled(false);

        panelButtons = new JPanel(new GridLayout(1, 2));
        panelButtons.add(buttonStart);
        panelButtons.add(buttonStop);

        panelLabels = new JPanel(new BorderLayout());
        topPanel = new JPanel(new BorderLayout());
        topPanel.add(nrAppLabel, BorderLayout.NORTH);
        topPanel.add(itLabel, BorderLayout.CENTER);
        topPanel.add(appNameLabel, BorderLayout.SOUTH);
        panelLabels.add(topPanel, BorderLayout.NORTH);


        frame.add(labelTime, BorderLayout.CENTER);
        frame.add(panelLabels, BorderLayout.NORTH);
        frame.add(panelButtons, BorderLayout.SOUTH);
    }


    /**
     * The Label which shows the current application number gets updated
     *
     * @param appNumber (Application number)
     */
    public void updateNrAppLabel(int appNumber) {
        this.nrAppLabel.setText("Application number: " + String.valueOf(appNumber));
    }

    /**
     * The Label which shows the current application name gets updated
     *
     * @param appName (Name of the application)
     */
    public void updateAppNameLabel(String appName) {
        this.appNameLabel.setText("Package Name: " + appName);
    }

    /**
     * The Label which shows the current iteration number gets updated
     *
     * @param iteration (Iteration number)
     */
    public void updateIterationLabel(int iteration) {
        this.itLabel.setText("Iteration number: " + String.valueOf(iteration));
    }


}