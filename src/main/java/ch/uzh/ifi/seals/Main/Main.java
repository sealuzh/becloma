package ch.uzh.ifi.seals.Main;

import ch.uzh.ifi.seals.Clustering.*;
import ch.uzh.ifi.seals.Devices.Tablet;
import ch.uzh.ifi.seals.Linking.FileUtilities;
import ch.uzh.ifi.seals.Linking.Preprocessing.DuplicateRemoval;
import ch.uzh.ifi.seals.Linking.Preprocessing.PreprocessingFacade;
import ch.uzh.ifi.seals.Linking.Review;
import ch.uzh.ifi.seals.Linking.ReviewParser;
import ch.uzh.ifi.seals.Linking.metrics.DiceCoefficient;
import ch.uzh.ifi.seals.Linking.metrics.SimilarityMetricsFacade;
import ch.uzh.ifi.seals.Parsers.PreProcessedReportParser;
import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.ClusteringManager;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.config.Exceptions.ArgsExceptions;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class Main {

    public static long AVD_BOOT_DELAY;
    public static boolean IS_SAPIENZ;
    public static boolean IS_MONKEY;
    public static boolean IS_EMULATOR;
    public static boolean IS_DEVICE;
    public static boolean HAS_TIMER;
    public static String REPORT_DIRECTORY;

    private static Chronometer chronometer;

    public static void main(String[] args) throws Exception {
        validateArgs(args);
        setGlobalVariables(args);

        CrashLogParser parser = new CrashLogParser();
        File[] crash_container = new File(ConfigurationManager.getInstance().getCrashContainerDirectory()).listFiles();
        for (File crash : crash_container) {
            parser.parseCrashLog(crash);
        }

        // the package which is going to be investigated
        String packageName = ClusteringManager.getInstance().getPackageName();

        CrashLogCollector crashLogCollector = new CrashLogCollector();
        Map<String, List<CrashLog>> crashLogsMap = crashLogCollector.getCrashLogsMap();
        List<CrashLog> crashLogs = crashLogsMap.get(packageName);
        TFIDF tfidf = new TFIDF(crashLogs);
        //tfidf.printScoreMap(tfidf.getScoreMap());

        HashMap<String, HashMap> scoreMap = tfidf.getScoreMap();

        CrashLogComparator crashLogComparator = new CrashLogComparator(scoreMap);
        UniqueCrashesCollector u = new UniqueCrashesCollector(crashLogs, scoreMap);
        crashLogComparator.compareCrashLogsSimiliarity();
        u.fillUniqueCrashesCollector();

        // unique crashes separated by group
        HashMap<String, List<CrashLog>> crashLogBucket = u.getCrashLogBucket();
        //u.printCrashLogBucket();


        // Key: packagename, Value: list of reviews
        HashMap<String, List<Review>> reviewMap = ReviewParser.getReviewMap();
//
//        reviewMap.forEach((packageName, reviews) -> {
//            System.out.println(packageName);
//
//            reviews.forEach(review -> {
//                System.out.println("\t\t" + review.toString());
//            });
//        });


        /**
         * Linking process
         */
        // print the unique crash_logs
        final String[] content = {""};
        crashLogBucket.forEach((crash_log_location, crash_log_list) -> {
            for (CrashLog crashLog : crash_log_list) {

                // Step 1: Augment Stack Traces
                // preprocess the set of all words contained into the stack_trace
                String preprocessedWords = PreprocessingFacade.preprocess(crashLog.getSetOfWords());
                String words[] = preprocessedWords.split("\\s");
                crashLog.setSetOfWords(new ArrayList<>(Arrays.asList(words)));
                // preprocess the cause of the raised exception
                String afterPreprocessing = crashLog.getAfterPreprocessing();
                ArrayList<String> temp = new ArrayList<>();
                temp.add(afterPreprocessing);
                crashLog.setAfterPreprocessing(PreprocessingFacade.preprocess(temp));

                // preprocess all methods contained into the stack_trace
                String preprocess = PreprocessingFacade.preprocess(crashLog.getTriggerMethods());
                String methods[] = preprocessedWords.split("\\s");
                //  crashLog.setCrashLogMethods(new ArrayList<>(Arrays.asList(methods)));
                // preprocess augmented stack trace
                String augmentedStackTraces = crashLog.getAugmentedStackTraces();
                temp.clear();
                temp.add(augmentedStackTraces);
                String withoutDuplicates = DuplicateRemoval.removeDuplicates(PreprocessingFacade.preprocess(temp) + crashLog.getAfterPreprocessing());
                crashLog.setAugmentedStackTraces(withoutDuplicates);
                String convertToList[] = crashLog.getAugmentedStackTraces().split("\\s");


                // Step 2: Calculate Dice similarity between the augmented stack trace and each preprocessed review
                List<Review> reviewList = reviewMap.get(crashLog.getPackageName());
                content[0] += "CrashLogPath:  " + crashLog.getCrashLogLocation() + ", " + "\n\n";
                for (Review review : reviewList) {
                    List<String> preProcessedReview = review.getPreProcessedReview();
                    List<String> augmentedStackTrace = new ArrayList<>(Arrays.asList(convertToList));
                    // double diceIndex = DiceCoefficient.calculateDiceCoefficient(preProcessedReview, augmentedStackTrace);
                    double diceIndex = SimilarityMetricsFacade.evaluateAsimmetricDiceIndex(preProcessedReview, augmentedStackTrace);
                    if (diceIndex >= 0.5) {
                        crashLog.getReviewDiceScoreMap().put(review, diceIndex);
                        content[0] +=
                                "\n\t\treview_id:   " + review.getId() + ", "
                                        + "\n\t\tpreprocessed_review:   " + preProcessedReview.toString() + ", "
                                        + "\n\t\taugmented_stack_trace:  " + augmentedStackTrace.toString() + ", "
                                        + "\n\t\tdice score:   " + diceIndex
                                        + "\n\n";

                    }
                }

                // System.out.println(crashLog);

            }
        });
        CommandExecutor.toWriteString(content[0], packageName + ".csv");
        //FileUtilities.writeFile(content[0], "linking_danvelazco.csv");

//        crashLogBucket.forEach( (crash_path, crash_log) -> {
//            System.out.println();
//        });


//        System.out.println(PreProcessedReportParser.getClassesFromCSV());


    }


    private static void setGlobalVariables(String[] args) throws Exception {
        if (args[0].equalsIgnoreCase("-device")) {
            IS_DEVICE = true;
            IS_EMULATOR = false;
//            initializeDevices();
        } else {
            IS_DEVICE = false;
            IS_EMULATOR = true;
        }
        if (args[1].equalsIgnoreCase("-monkey")) {
            IS_MONKEY = true;
            IS_SAPIENZ = false;
            REPORT_DIRECTORY = ConfigurationManager.getInstance().getMonkeyReportsDirectory();
        } else {
            IS_MONKEY = false;
            IS_SAPIENZ = true;
            REPORT_DIRECTORY = ConfigurationManager.getInstance().getSapienzReportsDirectory();
        }
        if (args.length > 2) {
            HAS_TIMER = args[2].equalsIgnoreCase("-timer");
        }
    }

    public static void initializeDevices() throws IOException, InterruptedException {
        AVD_BOOT_DELAY = Long.valueOf(ConfigurationManager.getInstance().getAVDBootDelay());
        Tablet.rebootDevices();
        System.out.println("Waiting devices " + AVD_BOOT_DELAY + " seconds for rebooting...");
        Thread.sleep(TimeUnit.SECONDS.toMillis(AVD_BOOT_DELAY));
        Tablet.rootDevices();
        Thread.sleep(4000);
        Tablet.getDevicesReady();
        Thread.sleep(3000);
    }

    private static void validateArgs(String[] args) throws ArgsExceptions {
        if (args.length > 0) {
            final String TESTING_MODE = args[0];
            final String TESTING_TOOL = args[1];
            String CHRONOMETER = "";
            if (args.length == 3) {
                CHRONOMETER = args[2];
            }


            if (!TESTING_MODE.equalsIgnoreCase("-device") && !TESTING_MODE.equalsIgnoreCase("-emulator")) {
                throw new ArgsExceptions("The first given parameter [TESTING_MODE]" + " is not valid!" +
                        " Be sure that is it either -device or -emulator. \n" +
                        "Example:\t java my/Jar/Path -device -monkey");
            }
            if (!TESTING_TOOL.equalsIgnoreCase("-sapienz") && !TESTING_TOOL.equalsIgnoreCase("-monkey")) {
                throw new ArgsExceptions("The second given parameter [TESTING_TOOL]" + " is not valid!" +
                        " Be sure that is it either -sapienz or -monkey. \n" +
                        "Example:\t java my/Jar/Path -device -monkey");
            }

            if (!CHRONOMETER.equalsIgnoreCase("-timer") && !CHRONOMETER.equals("")) {
                throw new ArgsExceptions("The third parameter given [CHRONOMETER]" + " is not valid!" +
                        " Be sure that is it either -timer or nothing. \n" +
                        "Example:\t java my/Jar/Path -device -monkey -timer");
            }
        } else if (args.length != 2) {
            throw new ArgsExceptions("At least two parameters are required! [TESTING_MODE],[TESTING_TOOL] " +
                    "Example:\t java my/Jar/Path -device -monkey");
        } else {
            throw new ArgsExceptions("\n\nNo args are given! Please attach them with the " +
                    "following order: \n" +
                    "java <jar_path> <is_device/emulator> <is_sapienz/monkey> \n" +
                    "Example:\t java my/Jar/Path -device -monkey");
        }
    }

    private static void startChronometer() {
        if (HAS_TIMER) {
            chronometer = new Chronometer();
        }
    }

    public static Chronometer getChronometer() {
        return chronometer;
    }


}