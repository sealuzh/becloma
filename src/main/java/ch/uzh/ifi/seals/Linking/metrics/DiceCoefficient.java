package ch.uzh.ifi.seals.Linking.metrics;

import java.util.ArrayList;
import java.util.List;

/**
 * This class links the collected crash_logs with the collected_reviews
 * Created by LuckyP on 18.05.17.
 */
public abstract class DiceCoefficient {


    public static double calculateDiceCoefficient(List<String> preProcessedReview, List<String> augmentedStackTraceWords) {
        return getCommonWordsSize(preProcessedReview, augmentedStackTraceWords) / Math.min(preProcessedReview.size(), augmentedStackTraceWords.size());

    }

    private static double getCommonWordsSize(List<String> tokenizedReviewWords, List<String> augmentedStackTraceWords) {
        List<String> commonWords = new ArrayList<>(tokenizedReviewWords);
        commonWords.retainAll(augmentedStackTraceWords);
       // System.out.println(commonWords);
        return commonWords.size();
    }
}
