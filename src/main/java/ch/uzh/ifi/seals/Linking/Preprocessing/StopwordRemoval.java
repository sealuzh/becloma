package ch.uzh.ifi.seals.Linking.Preprocessing;

import ch.uzh.ifi.seals.Tester.CommandExecutor;

import java.io.IOException;
import java.util.ArrayList;


public class StopwordRemoval {

    public static String applyStopwordList(String pText) {
        pText = pText.replaceAll("[ ]{2,}", " ");
        String[] words = pText.split(" ");

        ArrayList<String> remainingWords = new ArrayList<>();
        ArrayList<String> stopWords = CommandExecutor.toReadArrayList("stopword.txt");

        String toReturn = "";

        for (String w : words) {
            boolean toRemove = false;
            if (stopWords.contains(w)) {
                toRemove = true;
            }

            if (!toRemove)
                remainingWords.add(w.toLowerCase());
        }

        for (String word : remainingWords) {
            toReturn += word + " ";
        }

        return toReturn;

    }
}
