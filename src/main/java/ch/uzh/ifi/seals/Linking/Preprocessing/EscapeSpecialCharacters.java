package ch.uzh.ifi.seals.Linking.Preprocessing;

public class EscapeSpecialCharacters {

	public static String espaceSpecialCharacters(String pText) {
		pText = pText.replaceAll("[^\\w\\*]", " ");
		pText = pText.replaceAll("[0-9]+", " ");
		
		return pText;
	}
}
