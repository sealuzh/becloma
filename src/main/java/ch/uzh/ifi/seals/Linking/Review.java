package ch.uzh.ifi.seals.Linking;

import java.util.List;

/**
 * Created by LuckyP on 17.05.17.
 */
public class Review {

    private String id;
    private String reviewedPackage;
    private String submittedDate;
    private String reviewText;
    private int stars; // acceptance (popularity) of the review
    private List<String> preProcessedReview;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReviewedPackage() {
        return reviewedPackage;
    }

    public void setReviewedPackage(String reviewedPackage) {
        this.reviewedPackage = reviewedPackage;
    }

    public String getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(String submittedDate) {
        this.submittedDate = submittedDate;
    }

    public List<String> getPreProcessedReview() {
        return preProcessedReview;
    }

    public void setPreProcessedReview(List<String> preProcessedReview) {
        this.preProcessedReview = preProcessedReview;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    @Override
    public String toString() {
        return "Review  {" + "\n" +
                "\t\tid='" + id + '\'' + "\n" +
                "\t\treviewedPackage='" + reviewedPackage + '\'' + "\n" +
                "\t\tsubmittedDate='" + submittedDate + '\'' + "\n" +
                "\t\treviewText='" + reviewText + '\'' + "\n" +
                "\t\tstars='" + stars + '\'' + "\n" +
                "\t\tpreProcessedReview=" + preProcessedReview + "\n" +
                '}';
    }

}
