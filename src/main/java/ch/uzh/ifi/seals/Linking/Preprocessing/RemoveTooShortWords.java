package ch.uzh.ifi.seals.Linking.Preprocessing;

public class RemoveTooShortWords {

	public static String removeTooShortWords(String pText) {
		String[] terms = pText.split(" ");
		String newText="";
		
		for(String term: terms) {
			if(term.length() > 3) {
				newText+=term+" ";
			}
		}
		
		return newText;
	}
}
