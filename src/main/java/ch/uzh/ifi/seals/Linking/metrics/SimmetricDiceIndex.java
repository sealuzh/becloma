package ch.uzh.ifi.seals.Linking.metrics;

import java.util.regex.Pattern;

/**
 * Created by LuckyP on 02.06.17.
 */
public class SimmetricDiceIndex {
    public static double computeSimmetricJaccardIndex(String pDocumentOne, String pDocumentTwo) {
        Pattern space = Pattern.compile(" ");

        String[] docOneWords = space.split(pDocumentOne);
        String[] docTwoWords = space.split(pDocumentTwo);

        double overlap = SimmetricDiceIndex.overlapWords(docOneWords, docTwoWords);
        double totalWords = docOneWords.length + docTwoWords.length;

        return (overlap/(totalWords-1));
    }

    private static double overlapWords(String[] pDocOne, String[] pDocTwo) {
        double counter=0.0;

        for(String word: pDocOne) {
            for(String wordTwo: pDocTwo) {
                if(word.equals(wordTwo))
                    counter++;
            }
        }

        return counter;
    }
}
