package ch.uzh.ifi.seals.Linking.metrics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuckyP on 02.06.17.
 */
public class SimilarityMetricsFacade {

    /**
     * Method to compute the simmetric Dice index
     *
     * @param pReview: review (pre-processed or not) to analyze
     * @param pClass:  source code (pre-processed or not) of the
     *                 code component to analyze
     * @return a double value representing the similarity
     * between the given texts
     */
    public static double evaluateSimmetricJaccardIndex(String pReview, String pClass) {
        return SimmetricDiceIndex.computeSimmetricJaccardIndex(pReview, pClass);
    }

    /**
     * Method to compute the asimmetric Dice index
     *
     * @param pReview: review (pre-processed or not) to analyze
     * @param augmentedStackTrace: stack trace which has been already augmented with the source code
     * @return a double value representing the similarity
     * between the given texts
     */
    public static double evaluateAsimmetricDiceIndex(List<String> pReview, List<String> augmentedStackTrace) {
        return AsimmetricDiceIndex.computeAsimmetricDiceIndex(pReview, augmentedStackTrace);
        //return AsimmetricDiceIndex.computeAsimmetricDiceIndex(pReview, pClass);
    }
}
