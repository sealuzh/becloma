package ch.uzh.ifi.seals.Linking.metrics;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by LuckyP on 02.06.17.
 */
public class AsimmetricDiceIndex {

    public static double computeAsimmetricDiceIndex(List<String> pDocumentOne, List<String> pDocumentTwo) {
//        Pattern space = Pattern.compile(" ");
//
//        String[] docOneWords = space.split(pDocumentOne);
//        String[] docTwoWords = space.split(pDocumentTwo);

        Double overlap = AsimmetricDiceIndex.overlapWords(pDocumentOne, pDocumentTwo);
        Double result = 0.0;

        if (pDocumentTwo.size() < pDocumentOne.size())
            result = ((overlap * 2) / pDocumentTwo.size());
        else result = ((overlap * 2) / pDocumentOne.size());

        if (result.doubleValue() > 1.0)
            return 1.0;

        if (result.isInfinite())
            return 0.0;

        if (result.toString().contains("E"))
            return 0.0;

        return result;
    }

    private static double overlapWords(List<String> pDocOne, List<String> pDocTwo) {
        double counter = 0.0;

        for (String word : pDocOne) {
            for (String wordTwo : pDocTwo) {
                if (word.equals(wordTwo)) {
                    counter++;
                }
            }
        }

        return counter;
    }
}
