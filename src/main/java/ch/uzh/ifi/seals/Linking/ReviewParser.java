package ch.uzh.ifi.seals.Linking;

import ch.uzh.ifi.seals.Linking.Review;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import com.sun.org.apache.regexp.internal.RE;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by LuckyP on 17.05.17.
 */
public abstract class ReviewParser {


    private static final String DELIMITER = ",";
    private static final String DATASET_DIRECTORY_PATH = ConfigurationManager.getInstance().getReviewDirectory();
    private static HashMap<String, List<Review>> reviewsMap = new HashMap<>();


    /**
     * Stream gets initialized for this class
     *
     * @throws IOException
     */
    private static Stream<String[]> getStream() throws IOException {
        return Files.lines(Paths.get(DATASET_DIRECTORY_PATH))
                .filter(line -> !line.startsWith("id") && isInUTF8Encoded(line, "UTF-8"))
                .map(line -> line.split(DELIMITER));
    }

    private static void initializeReviewMap() throws IOException {
        getStream().forEach(line_split -> {
            String packageName = line_split[1];
            if (!reviewsMap.containsKey(packageName)) {
                reviewsMap.put(packageName, new ArrayList<>());
            }
        });
    }

    public static HashMap<String, List<Review>> getReviewMap() throws IOException {
        initializeReviewMap();
        getStream().forEach(line_split -> {
            Review review = new Review();
            review.setId(line_split[0]);
            review.setReviewedPackage(line_split[1]);
            review.setReviewText(line_split[2]);
            review.setSubmittedDate(line_split[3]);
            review.setStars(Integer.parseInt(line_split[4]));
            List<String> tokenizedWords = new ArrayList<>();
            tokenizedWords.addAll(Arrays.asList(line_split[5].split("\\s")));
            review.setPreProcessedReview(tokenizedWords);
            reviewsMap.get(review.getReviewedPackage()).add(review);
        });

        return reviewsMap;
    }

    public static boolean isInUTF8Encoded(String input, String encoding) {
        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        CharsetEncoder encoder = Charset.forName(encoding).newEncoder();
        ByteBuffer tmp;
        try {
            tmp = encoder.encode(CharBuffer.wrap(input));
        } catch (CharacterCodingException e) {
            return false;
        }

        try {
            decoder.decode(tmp);
            return true;
        } catch (CharacterCodingException e) {
            return false;
        }
    }


}
