package ch.uzh.ifi.seals.Linking.Preprocessing;

public class ComposedIdentifierSplitting {

	public static String splitWord(String pWord) {
		String newWords="";
		
		newWords = ComposedIdentifierSplitting.splitCamelCaseWord(pWord);
		newWords = ComposedIdentifierSplitting.splitUnderScoredWord(newWords);
		newWords = ComposedIdentifierSplitting.splitDigitSeparatedWord(newWords);
		
		return newWords;
	}
	
	private static String splitCamelCaseWord(String pWord) {
		String newWords="";
		
		for(String word: pWord.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])"))
			newWords+=word + " ";
		
		return newWords;
	}
	
	private static String splitUnderScoredWord(String pWord) {
		String newWords="";
		
		for(String word: pWord.split("_"))
			newWords+=word + " ";
		
		return newWords;
	}
	
	private static String splitDigitSeparatedWord(String pWord) {
		String newWords="";
		
		for(String word: pWord.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)"))
			newWords+=word + " ";
		
		return newWords;
	}
}
