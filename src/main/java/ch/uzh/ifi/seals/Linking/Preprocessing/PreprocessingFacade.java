package ch.uzh.ifi.seals.Linking.Preprocessing;

import java.util.ArrayList;

/**
 * This class preprocess the given text
 */
public abstract class PreprocessingFacade {
	
	public static String preprocess(ArrayList<String> words) {
		String newText="";
//		String words[] = pText.split(" ");
		
		// step 1: splitting words (separated with capital letters, underscores, numbers)
		for(String word: words) {
			newText+=ComposedIdentifierSplitting.splitWord(word) + " ";
		}
		
		// step 2: reducing all the words to lower case
		newText = newText.toLowerCase();
		
		// step 3: remove special characters
		newText = EscapeSpecialCharacters.espaceSpecialCharacters(newText);
		
		// step 4: stopword list application
		newText = StopwordRemoval.applyStopwordList(newText);
		
		// step 5: stemming words using Porter stemmer
		// newText = PorterStemmer.getInstance().stemString(newText);
		
		// step 6: remove duplicates
		newText = DuplicateRemoval.removeDuplicates(newText);
				
		// step 7: remove words having too few characters
		newText = RemoveTooShortWords.removeTooShortWords(newText);
				
		return newText;
	}
}