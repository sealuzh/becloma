package ch.uzh.ifi.seals.Linking.Preprocessing;

public class DuplicateRemoval {

	public static String removeDuplicates(String pText) {
		String[] terms = pText.split(" ");
		String newText="";
		
		for(String term: terms) {
			if(! newText.contains(term)) {
				newText+=term+" ";
			}
		}
		
		return newText;
	}
}
