package ch.uzh.ifi.seals.Tester;

import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.costants.TesterData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static ch.uzh.ifi.seals.Main.Main.IS_MONKEY;
import static ch.uzh.ifi.seals.Main.Main.IS_SAPIENZ;

/**
 * This class executes command lines + writes into a file and reads from it
 */
public class CommandExecutor {

    public static Process getProcess(String cmd) throws IOException {
        return Runtime.getRuntime().exec(cmd);
    }

    public static void createProcess(String cmd) throws IOException {
        Runtime.getRuntime().exec(cmd);
    }

    /**
     * Executes the thread with terminal view
     *
     * @param commandLine
     */
    public static void executeCommand(String commandLine) {
        Process p;
        System.out.println(commandLine);
        try {
            p = Runtime.getRuntime().exec(commandLine);
            //p.waitFor();
            BufferedReader reader;
            BufferedReader errorReader;
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                System.out.println(line); // for terminal view
            }
            String errorLine;
            while ((errorLine = errorReader.readLine()) != null) {
                System.out.println(errorLine); // for terminal view
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the result of the commandline's execution
     *
     * @param commandLine
     * @return String
     */
    public static String getOutputFromTerminal(String commandLine) {
        System.out.println(commandLine);
        StringBuilder stringBuilder = new StringBuilder();
        String terminalOutput = "";
        Process p;
        try {
            p = Runtime.getRuntime().exec(commandLine);
            BufferedReader errorReader;
            BufferedReader reader;
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line;
            while ((line = errorReader.readLine()) != null) {
                System.out.println(line);
                stringBuilder.append(line);
            }
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                stringBuilder.append(line + "\n");
            }

            terminalOutput = stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return terminalOutput;
    }

    /**
     * Generates the reports and save the output into a Textfile
     *
     * @param commandLine    the command line to start the execution
     * @param outputFileName the output file name
     */
    public static void generateReport(String commandLine, String outputFileName, String deviceName) throws Exception {
        System.out.println(commandLine);
        Runtime rt = Runtime.getRuntime();
        Process p = rt.exec(commandLine);
        Long timeout = 0L;
        if (IS_MONKEY) {
            timeout = ConfigurationManager.getInstance().monkeyTimeout();
        } else if (IS_SAPIENZ) {
            timeout = ConfigurationManager.getInstance().sapienzTimeout();
        }
        StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), commandLine, "ERROR - " + deviceName, outputFileName, deviceName);
        StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), commandLine, deviceName, outputFileName, deviceName);
        errorGobbler.start();
        outputGobbler.start();

        // Timeout is set
        if (!p.waitFor(timeout, ConfigurationManager.getInstance().TimeoutUnit())) {
            System.err.println("############# A Timeout occurred! Process is going to be destroyed....");
            System.err.println("Report file gets updated...");
            PrintWriter writer = outputGobbler.getWriter();
            writer.append("\n\n===================== A TIMEOUT EXCEPTION OCCURRED =====================");
            p.destroy();
            Thread.sleep(2000);
        }
        writeTestingEndTime(outputFileName);
    }

    private static void writeTestingEndTime(String outputFileName) throws IOException, ParseException {
        File file = new File(outputFileName);
        List<String> lines = Files.readAllLines(file.toPath());
        DateFormat formatter = TesterData.getFormat();
        Date startTesting = formatter.parse(lines.get(1).split("Testing Start Time:")[1].trim());
        Date endTesting = formatter.parse(TesterData.getCurrentTime());
        Long differenceMilliseconds = endTesting.getTime() - startTesting.getTime();
        String differenceFormatted = TimeUnit.MILLISECONDS.toSeconds(differenceMilliseconds) + " seconds (" + TimeUnit.MILLISECONDS.toMinutes(differenceMilliseconds) + " minutes)";
        lines.add(2, "Testing End Time: " + TesterData.getCurrentTime());
        lines.add(3, "Total Testing Time: " + differenceFormatted);
        Files.write(file.toPath(), lines, StandardCharsets.UTF_8);
        System.out.println("Testing End Time has been written");
    }

    /**
     * Executes a thread without terminal view
     *
     * @param commandLine String
     */
    public void executeCommandWithoutTerminalView(String commandLine) {
        System.out.println(commandLine);
        try {
            Runtime.getRuntime().exec(commandLine);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(commandLine + "        FAILS!!!!");
        }
    }

    public static <T> void toWriteArrayList(ArrayList<T> arrayList, String outputFileName) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(outputFileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for (T i : arrayList) {
            writer.write(i + "\n");
        }
        writer.close();

    }

    /**
     * example
     * public <T extends Number, S extends Number> T a(T b, S c) {
     * return b;
     * }
     */

    public static void toWriteString(String string, String outputFileName) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(outputFileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.write(string);
        writer.close();

    }

    public static String toRead(String inputFileName) {
        File file = new File(inputFileName);
        String reportGeneration = "";
        try {
            // alternative code
            // reportGeneration = Files.lines(file.toPath()).toString();

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                reportGeneration = line;
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return reportGeneration;
    }

    public static ArrayList<String> toReadArrayList(String inputFileName) {
        ArrayList<String> output = new ArrayList<>();
        File file = new File(inputFileName);
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                output.add(line);
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }

    public <T> void printDataSet(ArrayList<T> packages) {
        for (T _package : packages) {
            System.out.println(_package);
        }
    }

    public int countFilesInFolder(String filePath) {
        File file = new File(filePath);

        if (!file.exists()) {
            System.err.println(file.getAbsolutePath() + " doesn't exist");
            return -1;
        }
        File[] files = file.listFiles();
        int nrOfFiles = 0;
        assert files != null;
        for (File f : files) {
            nrOfFiles++;
        }

        return nrOfFiles;
    }

    public int countSapienzCrashLogs(String directoryPath) {
        int crashCounter = 0;
        File[] files = new File(directoryPath).listFiles();
        for (File crash : files) {
            if (crash.getName().startsWith("bugreport")) {
                crashCounter += 1;
            }
        }
        return crashCounter;
    }

    /**
     * @param directoryPath
     * @return
     */
    public int countNrOfMonkeyCrashFilesInsideAFolder(String directoryPath) {
        int crashCounter = 0;
        File testGenerationDir = new File(directoryPath);
        ArrayList<File> testGenFiles = new ArrayList<>(Arrays.asList(testGenerationDir.listFiles()));
        if (testGenerationDir.exists()) {
            for (File crashLog : testGenFiles) {
                if (crashLog.getName().contains("crash_log")) {
                    crashCounter++;
                }
            }
        } else {
            return -1;
        }

        return crashCounter;
    }

    /**
     * counts the number of monkey crashes, that are stored inside a crash_log file
     *
     * @param crashLogPath
     * @return
     */

    // TODO: STUPIDO MODO PER CONTARE I CRASH_LOG DENTRO A UN MONKEY LOG -> USARE LA CLASS GREP
    public int countNrOfMonkeyCrashInsideACrashLog(String crashLogPath) {
        ArrayList<String> crashLog = toReadArrayList(crashLogPath);
        String numberOfCrashes = null;
        for (String crash_line : crashLog) {
            if (crash_line.contains("Crash number")) {
                String temp[] = crash_line.split(":");
                numberOfCrashes = temp[temp.length - 1].trim();
            }
        }
        assert numberOfCrashes != null;
        return Integer.valueOf(numberOfCrashes);
    }


}
