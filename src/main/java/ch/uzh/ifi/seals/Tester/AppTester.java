package ch.uzh.ifi.seals.Tester;


import ch.uzh.ifi.seals.Devices.Emulator;
import ch.uzh.ifi.seals.Devices.Multithreading;
import ch.uzh.ifi.seals.Devices.Tablet;
import ch.uzh.ifi.seals.Experiment.DynamicParameters;
import ch.uzh.ifi.seals.Installer.Installer;
import ch.uzh.ifi.seals.Installer.MultiInstaller;
import ch.uzh.ifi.seals.Installer.MultiUninstaller;
import ch.uzh.ifi.seals.Main.Main;
import ch.uzh.ifi.seals.Parsers.Grep;
import ch.uzh.ifi.seals.Parsers.SapienzSettingsParser;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.config.Exceptions.DirectoryExceptions;
import ch.uzh.ifi.seals.config.Exceptions.ParametersExceptions;
import ch.uzh.ifi.seals.costants.CommandLines;
import ch.uzh.ifi.seals.costants.Enumerations.Extensions;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static ch.uzh.ifi.seals.Main.Main.*;

/**
 * This Class AppTester manages all the testing session for this project. It scans the <config.properties> file and
 * according to the given parameters it starts the right tool.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 17.12.2016
 */
public class AppTester {

    private ConfigurationManager config;
    private CommandExecutor commandExecutor;
    private File[] apksDirectory;
    private int numberOfIteration;

    public AppTester() throws InterruptedException, IOException {
        this.config = ConfigurationManager.getInstance();
        this.commandExecutor = new CommandExecutor();
        this.apksDirectory = new File(config.getSaveApkDirectory()).listFiles();
        this.numberOfIteration = Integer.valueOf(config.getNumberOfIterations());
        new Installer();
    }

    private static void updateChronometer(int appCounter, String apk) {
        if (HAS_TIMER) {
            Main.getChronometer().updateAppNameLabel(apk);
            Main.getChronometer().updateNrAppLabel(appCounter);
        }
    }

    private static void printCurrentTime(long start, String packageName) {
        long currentTime = System.nanoTime() - start;
        String format = String.format("%02d min",
                TimeUnit.NANOSECONDS.toMinutes(currentTime)
        );
        System.err.println(packageName + " has been tested for: " + format + " (of " + ConfigurationManager.getInstance().minutesPerApp() + " minutes)");
    }

    public void testAllApp() throws Exception {
        if (IS_MONKEY == IS_SAPIENZ) {
            throw new ParametersExceptions("Please select at least/only one tool in the config manager! Set True either IS_MONKEY or IS_SAPIENZ");
        }
        if (IS_DEVICE == IS_EMULATOR) {
            throw new ParametersExceptions("Please select at least/only one device/emulator in the config manager! Set True either IS_DEVICE or IS_EMULATOR");
        }
        if (IS_MONKEY) {
            if (areParametersValid()) {
                runMonkey();
            } else {
                throw new ParametersExceptions("The inserted parameters for Monkey are not valid!");
            }
        }
        if (IS_SAPIENZ) {
            runSapienz();
        }
    }

    /**
     * It checks whether the inserted parameters for monkey in the <config.properties> file are valid or not
     *
     * @return
     */
    private boolean areParametersValid() {
        int motion = Integer.parseInt(this.config.getPercentageMotionEvent().trim());
        int touch = Integer.parseInt(this.config.getPercentageTouchEvent().trim());
        int system = Integer.parseInt(this.config.getPercentageSystemEvent().trim());
        if (motion < 0 || motion > 100 || touch < 0 || touch > 100 || system < 0 || system > 100) {
            System.err.println("Monkey parameters: Percentages are not in the range from 0 to 100");
            return false;
        }
        if ((motion + touch + system) > 100) {
            System.err.println("Monkey parameters: Sum of Percentages are not in the range from 0 to 100");
            return false;
        }
        return true;
    }

    /**
     * The given APK is tested and its logs file is saved on the given TestGeneration Folder.
     *
     * @param NEW_TESTGENERATION_FOLDER (The new TestGeneration folder where all current logs are going to be saved)
     * @param APK_NAME                  (The given APK under test)
     * @param logCounter                (counts how many times an application has been tested inside the timer interval)
     * @throws InterruptedException
     * @throws IOException
     */
    private void testAppWithMonkey(final File NEW_TESTGENERATION_FOLDER, final String APK_NAME, int logCounter) throws Exception {
        ArrayList<Thread> threads = new ArrayList<>();
        ArrayList<File> monkeyLogFiles = new ArrayList<>();
        final String PACKAGE_NAME = APK_NAME.split("_")[0];

        uninstallApp(PACKAGE_NAME);
        installApp(APK_NAME);


        File newAppDir = new File(NEW_TESTGENERATION_FOLDER.getAbsolutePath() + '/' + PACKAGE_NAME); // a new folder its created with the name of the package
        if (!newAppDir.exists()) {
            newAppDir.mkdir();
        }

        threads.clear();
        // allow multithreading
        for (String device : Tablet.getAllAttachedDevices()) {
            File monkeyLogFile = new File(newAppDir.getAbsolutePath() + '/' + PACKAGE_NAME + "_MonkeyLog_" + device + "_" + logCounter + Extensions.TEXT.prefix());
            if (numberOfIteration > 1) {
                Multithreading thread = new Multithreading(CommandLines.getDynamicMonkeyCommandLine(PACKAGE_NAME, device, DynamicParameters.iterationValue), monkeyLogFile, device);
                threads.add(thread.start());
            } else {
                Multithreading thread = new Multithreading(CommandLines.getMonkeyCommandLine(PACKAGE_NAME, device), monkeyLogFile, device);
                threads.add(thread.start());
            }
            monkeyLogFiles.add(monkeyLogFile);
        }

        // waits the "testing" thread to complete
        for (Thread thread : threads) {
            thread.join();
        }

        //generate crash log if it exists (if there is at least one crash in the log file)
        for (File monkeyLogFile : monkeyLogFiles) {
            if (hasCrash(monkeyLogFile.getAbsolutePath())) {
                generateMonkeyCrashLog(monkeyLogFile, logCounter);
                System.err.println("CRASH detected in " + monkeyLogFile);
            } else {
                System.err.println("NO CRASHES detected in " + monkeyLogFile);
            }
        }
        sleepDevice();
    }

    private void installApp(String APK_NAME) throws IOException, InterruptedException {
        ArrayList<Thread> threads = new ArrayList<>();
        for (String device : Tablet.getAllAttachedDevices()) {
            MultiInstaller installer = new MultiInstaller(APK_NAME, device);
            threads.add(installer.start());
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }

    private void uninstallApp(String PACKAGE_NAME) throws IOException, InterruptedException {
        ArrayList<Thread> threads = new ArrayList<>();
        for (String device : Tablet.getAllAttachedDevices()) {
            MultiUninstaller uninstaller = new MultiUninstaller(PACKAGE_NAME, device);
            threads.add(uninstaller.start());
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }

    /**
     * Waiting time between one app and the other
     *
     * @throws InterruptedException
     */
    private void sleepDevice() throws InterruptedException {
        Long waitingTime = 5L;
        System.out.println("==================Waiting " + waitingTime + " seconds for the next application..==================\n\n\n");
        TimeUnit.SECONDS.sleep(waitingTime);
    }

    /**
     * It generates a monkey crash log extracting from the log the crashes.
     *
     * @param monkeyLogFile (monkey log file, which has just been generated)
     * @param logCounter    (counts how many times an application has been tested inside the timer interval)
     * @throws IOException
     */
    private void generateMonkeyCrashLog(File monkeyLogFile, int logCounter) throws IOException {
        String deviceName = monkeyLogFile.toString().split("/")[monkeyLogFile.toString().split("/").length - 1].split("_")[2];
        Grep grep = new Grep(monkeyLogFile.getAbsolutePath());
        commandExecutor.toWriteArrayList(grep.getCrashLog(), monkeyLogFile.getParent() + '/' + "crash_log_" + logCounter + "_" + deviceName + Extensions.TEXT.prefix());

    }

    /**
     * returns true if a given monkey log contains a crash inside it; otherwise false
     *
     * @param monkeyLogPath (
     * @return
     * @throws IOException
     */
    private boolean hasCrash(String monkeyLogPath) throws IOException {
        Grep grep = new Grep(monkeyLogPath);
        return grep.getNumberOfCrashes() != 0;
    }

    /**
     * this method starts the testing tool Sapienz and generate logs files in Reports/SapienzReports
     */
    private void runSapienz() throws Exception {
        // Update the settings file of sapienz
        SapienzSettingsParser.updateSettingSapienz();

        int newReportCounter = this.getLastExperimentIndex(new File(Main.REPORT_DIRECTORY)) + 1;
        final File NEW_TESTGENERATION_FOLDER = new File(Main.REPORT_DIRECTORY + "/" + "TestGeneration_" + newReportCounter + "/");
        NEW_TESTGENERATION_FOLDER.mkdir();

        int appCounter = 0;
        int logCounter = 0;

        for (File apk : this.apksDirectory) {
            if (apk.getName().endsWith(".apk") && !apk.isDirectory()) {
                System.err.println("\nTesting " + apk.getName() + ", application number: " + ++appCounter + "\n");
                Thread.sleep(100);
                updateChronometer(appCounter, apk.getName());
                for (long stop = System.nanoTime() + TimeUnit.MINUTES.toNanos(Long.valueOf(config.minutesPerApp())); stop > System.nanoTime(); ) {
                    testAppWithSapienz(NEW_TESTGENERATION_FOLDER, apk.getName(), ++logCounter);
                }
                logCounter = 0;
            }
        }
    }

    /**
     * this method starts the testing tool Monkey and generate logs files in Reports/MonkeyReports
     */
    private void runMonkey() throws Exception {
        if (IS_EMULATOR) {
            Emulator.runEmulator();
        }
        int nextTestGenerationIndex = this.getLastExperimentIndex(new File(Main.REPORT_DIRECTORY)) + 1;
        final File NEW_TESTGENERATION_FOLDER = new File(Main.REPORT_DIRECTORY + "/TestGeneration_" + nextTestGenerationIndex + "/");
        NEW_TESTGENERATION_FOLDER.mkdir();

        int appCounter = 0;
        int logCounter = 0;

        // test every apk contained inside the apk directory
        for (File apk : this.apksDirectory) {
            if (apk.getName().endsWith(".apk") && !apk.isDirectory()) {
                System.err.println("\nTesting " + apk.getName() + ", application number: " + ++appCounter + "\n");
                Thread.sleep(100);
                updateChronometer(appCounter, apk.getName());
                // timer for each app
                long start = System.nanoTime();
                for (long stop = System.nanoTime() + TimeUnit.MINUTES.toNanos(Long.valueOf(config.minutesPerApp())); stop > System.nanoTime(); ) {
                    testAppWithMonkey(NEW_TESTGENERATION_FOLDER, apk.getName(), ++logCounter);
                    printCurrentTime(start, apk.getName());
                    Thread.sleep(1000);
                }
                logCounter = 0;
                System.out.println(apk.getName() + " has been tested for " + config.minutesPerApp() + " minutes ");
            }
        }

        // Increments the iteration value for the experiment. In doing so, the percentages of motion/touch/system events get updated at each iteration
        if (Integer.valueOf(this.config.getNumberOfIterations()) > 1)
            DynamicParameters.iterationValue += 1;
    }

    /**
     * Test-method for testing Sapienz
     *
     * @param newDir
     * @param packageAPKName
     */
    public void testMethodSapienz(File newDir, String packageAPKName) {
        String directoryForApp = newDir + "/" + packageAPKName;
        String instrumented_app_dir = directoryForApp + "/" + packageAPKName + "_output";
        File newAppDir = new File(directoryForApp);
        File instrumentedFolder = new File(instrumented_app_dir);
        newAppDir.mkdir();
        instrumentedFolder.mkdir();
        File coverages = new File(instrumented_app_dir + "/coverages");
        File crashes = new File(instrumented_app_dir + "/crashes");
        File intermediate = new File(instrumented_app_dir + "/intermediate");
        coverages.mkdir();
        crashes.mkdir();
        intermediate.mkdir();


        int min = 0;
        int max = 5;
        Random random = new Random();
        int rand = random.nextInt(max - min + 1) + min;

        for (int i = 0; i < rand; i++) {
            ArrayList<String> test = new ArrayList<>();
            test.add("// CRASH");
            test.add("SOME CRASH");
            String outputPath = crashes.toString() + "/bugreport_" + i + ".crash";
            commandExecutor.toWriteArrayList(test, outputPath);
        }


    }

    private void testAppWithSapienz(final File NEW_TESTGENERATION_FOLDER, final String APK_NAME, int logCounter) throws Exception {
        final String PACKAGE_NAME = APK_NAME.split("_")[0];

        String deviceName = Tablet.getAllAttachedDevices().toString();

        String instrumented_app_dir = config.getSaveApkDirectory() + '/' + APK_NAME + "_output";
        File newAppDir = new File(NEW_TESTGENERATION_FOLDER.getAbsolutePath() + '/' + APK_NAME.split("_")[0]);
        if (!newAppDir.exists())
            newAppDir.mkdir();
        File newAppTimerDir = new File(NEW_TESTGENERATION_FOLDER.getAbsolutePath() + '/' +  APK_NAME.split("_")[0] + '/' + APK_NAME + "_" + logCounter);
        newAppTimerDir.mkdir();

        CommandExecutor.generateReport(CommandLines.getSapienzCommandLine(APK_NAME),
                newAppTimerDir.getAbsolutePath() + '/' + PACKAGE_NAME + "_SapienzLog" + Extensions.TEXT.prefix(), deviceName);

        /**
         * Copy intermediate / crashes / coverages into sapienz report directory
         */
        File file = new File(instrumented_app_dir);
        if (file.isDirectory() && file.exists()) {
            commandExecutor.executeCommandWithoutTerminalView("mv " + instrumented_app_dir + " " + newAppTimerDir.toString());
            System.out.println("\n\n-----" + instrumented_app_dir + " copied into " + newAppTimerDir.toString() + "------\n\n");
        } else {
            System.err.println("Error by coping " + instrumented_app_dir + " into " + newAppTimerDir.toString());
        }
    }

    /**
     * this methods is only used for simulate processes. It produces the same output as testAppWithMonkey() but
     * with fake data
     *
     * @param newDir
     * @param packageName
     */
    public void testMethodMonkey(File newDir, String packageName) {
        String packageFolderPath = newDir.toString() + '/' + packageName;
        File newPackageDir = new File(packageFolderPath);
        newPackageDir.mkdir();

        File allCrashDir = new File(newDir.toString() + "/ALL_CRASHES");
        allCrashDir.mkdir();

        ArrayList<String> test = new ArrayList<>();
        test.add("Crash number: 1");
        if (Math.random() < 0.5) {
            test.add("Crash number: 2");
        }

        if (Math.random() < 0.5 && test.size() != 2) {
            test.add("Crash number: 2");
            test.add("Crash number: 3");
        }


        if (Math.random() < 0.5) {
            commandExecutor.toWriteArrayList(test, packageFolderPath + "/crash_log_1.txt");
        }
    }


    /**
     * Parses all the directories into the monkey reports one and returns the index of the last experiment
     *
     * @param directory
     * @return
     */
    private int getLastExperimentIndex(File directory) throws DirectoryExceptions {
        List<Integer> nums = new ArrayList<>();
        for (File dir : directory.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY)) {
            if (dir.toString().contains("TestGeneration") || dir.toString().contains("ST"))
                nums.add(Integer.parseInt(dir.toString().split("_")[1]));
        }
        if (nums.isEmpty())
            return 0;
        else
            return Collections.max(nums);
    }

    /**
     * Tests single application
     * For Monkey: package name
     * For Sapienz: apk package name
     *
     * @param _package
     */
//    public void testApp(String _package) throws IOException {
//        String sapienzSingleTestReportDir = this.config.getSapienzReportsDirectory() + '/' + "SingleTests";
//        String monkeySingleTestReportDir = this.config.getMonkeyReportsDirectory() + '/' + "SingleTests";
//        if (Boolean.valueOf(this.config.IS_MONKEY())) {
//            int newReportCounter = this.getLastExperimentIndex(new File(monkeySingleTestReportDir)) + 1;
//            String newFolderName = "ST_" + newReportCounter;
//            String newDirPath = monkeySingleTestReportDir + "/" + newFolderName + "/";
//            File newDir = new File(newDirPath);
//            newDir.mkdir();
//            commandExecutor.generateReport(CommandLines.getMonkeyCommandLine(_package),
//                    newDirPath + _package + ".txt");
//            generateMonkeyCrashLog(newDir, _package + ".txt", 0);
//
//        } else if (Boolean.valueOf(this.config.IS_SAPIENZ())) {
//            int newReportCounter = this.getLastExperimentIndex(new File(sapienzSingleTestReportDir)) + 1;
//            String newFolderName = "ST_" + newReportCounter;
//            String newDirPath = sapienzSingleTestReportDir + "/" + newFolderName + "/";
//            File newDir = new File(newDirPath);
//            newDir.mkdir();
//            commandExecutor.generateReport(CommandLines.getSapienzCommandLine(_package),
//                    newDirPath + _package + ".txt");
//        }
//    }

}












