package ch.uzh.ifi.seals.Tester;

import ch.uzh.ifi.seals.costants.TesterData;

import java.io.*;

/**
 * Created by LuckyP on 14.04.17.
 */
public class StreamGobbler extends Thread {
    private InputStream is;
    private String type;
    private PrintWriter writer;
    private String outputPath;
    private String commandLine;
    private String deviceName;

    public StreamGobbler(InputStream is, String commandLine, String type, String outputPath, String deviceName) {
        this.is = is;
        this.type = type;
        this.outputPath = outputPath;
        this.commandLine = commandLine;
        this.deviceName = deviceName;
    }

    public void run() {
        try {
            writer = new PrintWriter(outputPath, "UTF-8");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            writer.append(TesterData.getMetaData(deviceName) + commandLine + "\n\n\n");
            while ((line = br.readLine()) != null) {
                System.out.println(type + " > " + line);
                writer.append(line).append("\n");
            }
            closeWriter();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public PrintWriter getWriter() {
        return this.writer;
    }

    public void closeWriter() {
        writer.close();
    }

}
