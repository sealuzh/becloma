package ch.uzh.ifi.seals.costants.Enumerations;

/**
 * This enumeration defines the different file extensions used in this project
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 03.04.2017
 */
public enum Extensions {

    CONFIG(".properties"),
    EXCEL(".xlsx"),
    TEXT(".txt"),
    CRASH(".crash"),
    ANDROID_APP(".apk"),
    EMULATOR(".avd"),
    SOURCE("_src"),
    ZIP(".tar.gz");

    private String extension;

    Extensions(String extension) {
        this.extension = extension;
    }

    public String prefix() {
        return extension;
    }
}
