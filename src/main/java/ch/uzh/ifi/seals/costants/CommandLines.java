package ch.uzh.ifi.seals.costants;

import ch.uzh.ifi.seals.Experiment.DynamicParameters;
import ch.uzh.ifi.seals.config.ConfigurationManager;

/**
 * This class builds some command lines that can be used for the testing session
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 17.12.2016
 */
public abstract class CommandLines {

    /**
     * public constants
     */
    public static final String START_FDROID = "am start -n org.fdroid.fdroid/org.fdroid.fdroid.FDroid";
    public static final String EMULATOR_LIST = "emulator -list-avds";
    public static final String NO_DEVICES_FOUND_MESSAGE = "error: no devices/emulators found";
    public static final String DEVICE_STATE = "adb get-state";
    public static final String EMULATOR_SHELL = "emulator -avd";
    public static final String ADB_SHELL = "adb shell";
    public static final String PACKAGES_LIST = "pm list packages -e";
    public static final String ADB_DEVICES = "adb devices";
    public static final String KILLALL_ADB = "killall adb";
    /**
     * private Monkey parameters
     */
//    private static final String MONKEY_ENVIRONMENT = "adb shell monkey";
    private static final String TOUCH_EVENTS = "--pct-touch";
    private static final String MOTION_EVENTS = "--pct-motion";
    private static final String SYSTEM_EVENTS = "--pct-syskeys";
    private static final String SAPIENZ_ENVIRONMENT = "python";
    private static final String SURF_ENVIRONMENT = "java";
    private static String IGNORE_CRASHES = " --ignore-crashes";
    private static String DELAY_BETWEEN_EVENTS = "--throttle";

    private static int newTouchEvents;
    private static int newMotionEvents;
    private static int newSystemEvents;


    public static String getFindAllGradle(String findAllGradle) {
        return "find " + ConfigurationManager.getInstance().getSaveSrcDirectory() + " -type f -name \"*build.gradle\"";
    }

    public static String getEmulatorName() {
        return ConfigurationManager.getInstance().getEmulatorName();
    }

    public static String getInstallApp(final String DEVICE_NAME) {
        return ConfigurationManager.getInstance().getAdbExecDirectory() + " -s " + DEVICE_NAME + " install " +
                ConfigurationManager.getInstance().getSaveApkDirectory() + "/";
    }

    public static String getUninstallApp(final String DEVICE_NAME) {
        return ConfigurationManager.getInstance().getAdbExecDirectory() + " -s " + DEVICE_NAME + " uninstall ";
    }

    /**
     * Builds and return the command line to execute the test
     * FOR MONKEY!
     *
     * @param packageName the package to test
     * @return the command line string
     */
    public static String getMonkeyCommandLine(String packageName, String deviceName) {
        ConfigurationManager config = ConfigurationManager.getInstance();
        if (!Boolean.valueOf(config.getIgnoreCrashes().toLowerCase())) {
            IGNORE_CRASHES = "".trim();
        }
        int convert = Integer.valueOf(config.getDelayBetweenEvents());

        return "adb -s " + deviceName + " shell monkey " + config.getPackageAllowed() + " " + packageName + " " +
                config.getVerbosity() + " " + DELAY_BETWEEN_EVENTS + " " + String.valueOf(convert) +
                " " + IGNORE_CRASHES + " " + TOUCH_EVENTS + " " + config.getPercentageTouchEvent() + " "
                + MOTION_EVENTS + " " + config.getPercentageMotionEvent() + " " + SYSTEM_EVENTS + " " +
                config.getPercentageSystemEvent() + " " + config.getNoRandomEvents();
    }

    /**
     * Builds and return the command line for monkey, but with dynamic parameters
     *
     * @param packageName
     * @return
     */
    public static String getDynamicMonkeyCommandLine(String packageName, String deviceName, int iterationValue) {
        ConfigurationManager config = ConfigurationManager.getInstance();
        if (!Boolean.valueOf(config.getIgnoreCrashes().toLowerCase())) {
            IGNORE_CRASHES = "".trim();
        }

        int convert = Integer.valueOf(config.getDelayBetweenEvents());
        setNewTouchEvents(DynamicParameters.getNewPercentageTouchEvents(iterationValue));
        setNewMotionEvents(DynamicParameters.getNewPercentageMotionEvents(iterationValue));
        setNewSystemEvents(DynamicParameters.getNewPercentageSystemEvents(iterationValue));

        return "adb -s " + deviceName + " shell monkey " + config.getPackageAllowed() + " " + packageName + " " +
                config.getVerbosity() + " " + DELAY_BETWEEN_EVENTS + " " + String.valueOf(convert) +
                " " + IGNORE_CRASHES + " " + TOUCH_EVENTS + " " + newTouchEvents + " "
                + MOTION_EVENTS + " " + newMotionEvents + " " + SYSTEM_EVENTS + " " +
                newSystemEvents + " " + config.getNoRandomEvents();
    }

    /**
     * Builds and return the command line to execute the test
     * FOR SAPIENZ!
     *
     * @param packageAPKName the package to test -> APK!
     * @return the command line string
     */
    public static String getSapienzCommandLine(String packageAPKName) {
        ConfigurationManager config = ConfigurationManager.getInstance();
        return SAPIENZ_ENVIRONMENT + " " + config.getSapienzWorkDirectory() + "/" + "main.py" + " "
                + config.getSaveApkDirectory() + "/" + packageAPKName;
    }


    /**
     * Builds and return the command line for using SURF
     *
     * @param inputXMLPath  (Path of the input XML file)
     * @param outputXMLPath (Path of the output XML file)
     * @return
     */
    public static String getSurfCommandLine(String inputXMLPath, String outputXMLPath) {
        ConfigurationManager config = ConfigurationManager.getInstance();
        return SURF_ENVIRONMENT + " -classpath " + "\"" + config.getSurfDirectory() + "/lib/*:" + config.getSurfDirectory() + "/SURF.jar" + "\" "
                + "org.surf.Main " + inputXMLPath + " " + outputXMLPath;
    }

    public static int getNewTouchEvents() {
        return newTouchEvents;
    }

    public static void setNewTouchEvents(int newTouchEvents) {
        CommandLines.newTouchEvents = newTouchEvents;
    }

    public static int getNewMotionEvents() {
        return newMotionEvents;
    }

    public static void setNewMotionEvents(int newMotionEvents) {
        CommandLines.newMotionEvents = newMotionEvents;
    }

    public static double getNewSystemEvents() {
        return newSystemEvents;
    }

    public static void setNewSystemEvents(int newSystemEvents) {
        CommandLines.newSystemEvents = newSystemEvents;
    }

}


