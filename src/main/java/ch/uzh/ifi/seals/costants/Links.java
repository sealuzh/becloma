package ch.uzh.ifi.seals.costants;

/**
 * This class makes available some Links which can be used for some HTTP-Requests
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 17.12.2016
 */
public abstract class Links {

    // FDroid Http Repo Link
    public static final String API_FDROID = "https://f-droid.org/repo/";

}
