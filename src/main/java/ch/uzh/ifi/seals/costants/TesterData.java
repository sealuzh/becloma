package ch.uzh.ifi.seals.costants;

import ch.uzh.ifi.seals.config.ConfigurationManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static ch.uzh.ifi.seals.Main.Main.*;

/**
 * This class provides some information about the Tester and the parameters used for the testing session.
 * These metadata can be used to make the testing logs clearer and more understandable
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 17.12.2016
 */
public abstract class TesterData {

    private static final String TESTER_NAME = "Lucas Pelloni";
    private static ConfigurationManager config = ConfigurationManager.getInstance();

    /**
     * Gets the current time in according to the format specified in the constructor
     *
     * @return a Date instance
     */
    public static String getCurrentTime() {
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.format(new Date());
    }

    public static SimpleDateFormat getFormat() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }

    /**
     * It builds and returns the Tester Meta data
     *
     * @return (String containing some information for the testing session)
     */
    public static String getMetaData(String deviceName) {
        String type_of_device = "testing apps on a hardware device";

        double motion;
        double system;
        double touch;
        double total = Double.parseDouble(config.getNoRandomEvents());

        if (Integer.valueOf(config.getNumberOfIterations()) > 1) {
            motion = CommandLines.getNewMotionEvents();
            system = CommandLines.getNewSystemEvents();
            touch = CommandLines.getNewTouchEvents();

        } else {
            motion = Double.parseDouble(config.getPercentageMotionEvent());
            system = Double.parseDouble(config.getPercentageSystemEvent());
            touch = Double.parseDouble(config.getPercentageTouchEvent());
        }

        if (IS_EMULATOR) {
            deviceName = config.getEmulatorName();
            type_of_device = "testing apps on virtual device";
        }

        if (IS_MONKEY) {
            return "Tester Name: " + TESTER_NAME + "\n" + "Testing Start Time: " + getCurrentTime() + "\n" +
                    "Type of testing: " + type_of_device + "\n" + "Name of device: " + deviceName + "\n\n" +
                    "Percentage of motion events: " + motion + "% " +
                    "(number of motion events: " + (int) (motion / 100 * total) + " of " + (int) total + " events)\n" +
                    "Percentage of system events: " + system + "% " +
                    "(number of system events: " + (int) (system / 100 * total) + " of " + (int) total + " events)\n" +
                    "Percentage of touch events: " + touch + "% " +
                    "(number of touch events: " + (int) (touch / 100 * total) + " of " + (int) total + " events)\n\n\n";
        } else if (IS_SAPIENZ) {
            return "Tester Name: " + TESTER_NAME + "\n" + "Testing Start Time: " + getCurrentTime() + "\n" +
                    "Type of testing: " + type_of_device + "\n" + "Name of device: " + deviceName + "\n\n" +
                    "SEQUENCE_LENGTH_MIN: " + config.getSequenceLengthMin() + "\n" + "SEQUENCE_LENGTH_MAX :" + config.getSequenceLengthMax()
                    + "\n" + "SUITE_SIZE :" + config.getSuiteSize() + "\n" + "POPULATION_SIZE: " + config.getPopulationSize() + "\n" +
                    "OFFSPRING_SIZE: " + config.getOffspringSize() + "\n" + "GENERATION: " + config.getGeneration() + "\n" +
                    "Crossover probability (CXPB): " + config.getCrossoverProbability() + "\n" + "Mutation probability: " + config.getMutationProbability() +
                    "\n\n\n";
        } else
            return "Tester data cannot be loaded";
    }


}