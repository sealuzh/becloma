package ch.uzh.ifi.seals.costants;

import java.util.concurrent.TimeUnit;

/**
 * Created by LuckyP on 14.04.17.
 */
public abstract class Timeout {

    public final static TimeUnit UNIT = TimeUnit.MINUTES;
    public final static Long MONKEY_TIMEOUT = 3L;
    public final static Long SAPIENZ_TIMEOUT = 35L;

}
