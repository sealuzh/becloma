package ch.uzh.ifi.seals.Clustering;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ch.uzh.ifi.seals.Main.Main.IS_SAPIENZ;
import static ch.uzh.ifi.seals.Main.Main.REPORT_DIRECTORY;

/**
 * Created by Lucas Pelloni on 31.03.17.
 */
public class CrashLogParser {

    private static List<CrashLog> crashLogs;


    public CrashLogParser() {
        crashLogs = new ArrayList<>();
    }

    public static List<CrashLog> getCrashLogs() {
        return crashLogs;
    }

    public void parseCrashLog(File crashLogFile) throws IOException {
        if (crashLogFile.getName().startsWith("crash_log") || crashLogFile.getName().startsWith("bugreport")) {
            try {
                BufferedReader in = new BufferedReader(new FileReader(crashLogFile.getAbsolutePath()));
                String line;
                int counter = 0; // if there is more than one crash_log in the given file, they can be anyway uniquely stored
                while ((line = in.readLine()) != null) {
                    if (line.startsWith("// CRASH")) {
                        counter++;
                        // here it is a new crash
                        ArrayList<String> logLines = new ArrayList<>();
                        String packageName = line.substring(10).split("\\s")[0].trim();

                        while (line.contains("//") && line.length() != 2) {
                            logLines.add(line);
                            line = in.readLine();
                            if (line == null) {
                                break;
                            }
                        }
                        CrashLog crashLog = new CrashLog(counter + "__" + crashLogFile.getAbsolutePath(), packageName, logLines);

                        // TODO: integrate in this process Lucene API
                        if (isUnique(crashLog)) {
                            crashLogs.add(crashLog);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isUnique(CrashLog targetCrashLog) {
        return true;
    }

    public void printAllCrashLogAttributes() {
        for (CrashLog crashLog : crashLogs) {
            System.out.println(crashLog.toString());
        }
    }
}