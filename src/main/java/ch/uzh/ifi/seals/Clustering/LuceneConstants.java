package ch.uzh.ifi.seals.Clustering;

/**
 * Created by LuckyP on 10.05.17.
 */
public class LuceneConstants {

    public static final String FIELD_CONTENT = "word";
    public static final String FIELD_ID = "id";
}
