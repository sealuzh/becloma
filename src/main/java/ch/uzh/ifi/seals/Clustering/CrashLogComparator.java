package ch.uzh.ifi.seals.Clustering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 15.05.2017
 */
public class CrashLogComparator {

    private List<String> keys;
    private HashMap<String, HashMap> scoreMap;

    public CrashLogComparator(HashMap<String, HashMap> scoreMap) {
        this.scoreMap = scoreMap;
        this.keys = new ArrayList<>();
        this.keys.addAll(scoreMap.keySet());
    }

    public void compareCrashLogsSimiliarity() {
        for (int i = 0; i < this.scoreMap.keySet().size(); i++) {
            for (int j = i + 1; j < this.scoreMap.keySet().size(); j++) {
                String document1 = this.keys.get(i);
                String document2 = this.keys.get(j);
                String d1 = document1.split("/")[document1.split("/").length - 1];
                String d2 = document2.split("/")[document2.split("/").length - 1];
                HashMap<String, Double> v1 = this.scoreMap.get(document1);
                HashMap<String, Double> v2 = this.scoreMap.get(document2);
                CosineSimilarityCalculator cosineSimilarityCalculator = new CosineSimilarityCalculator(document1.split("/")[document1.split("/").length - 1], document2.split("/")[document2.split("/").length - 1], v1, v2);
                System.out.println(d1 + " AND " + d2 + "    :    " + cosineSimilarityCalculator.getCosineSimilarity() + "\n");
            }
        }

    }
}
