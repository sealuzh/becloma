package ch.uzh.ifi.seals.Clustering;

import ch.uzh.ifi.seals.config.ConfigurationManager;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by LuckyP on 08.05.17.
 */
public class TFIDF {

    private List<CrashLog> crashLogList;
    private TFIDFSimilarity similarity;
    private String pathToIndex;
    private File indexDirectory;

    public TFIDF(List<CrashLog> crashLogList) throws IOException {
        this.crashLogList = crashLogList;
        this.similarity = new ClassicSimilarity();
        this.pathToIndex = ConfigurationManager.getInstance().getCrashContainerIndexDirectory();
        this.indexDirectory = new File(pathToIndex);
        this.writeIndex(crashLogList);
    }


    /**
     * It returnsthe term frequency of the given term in the given document
     */
    private double getTF(Long termFreq) {
        return this.similarity.tf(termFreq);
    }

    /**
     * It computes the Inverse Document frequency of a given term compared to all documents
     */
    private double getIDF(long docFreq, int numberOfDocuments) {
        return this.similarity.idf(docFreq, numberOfDocuments);
    }


    /**
     * It computes the TF_IDF of a given term, in a given document compared to all documents
     *
     * @param numberOfDocuments (the number of documents present in the collection)
     * @param termFreq          the frequency of a term within a document (TF)
     * @param docFreq           the number of documents which contain the term (IDF)
     * @return
     */
    private double getTFIDF(int numberOfDocuments, long termFreq, long docFreq) {
        return getTF(termFreq) * getIDF(docFreq, numberOfDocuments);
    }

    /**
     * If the index_directory is empty, a new index gets created with the given List of crash_logs
     */
    private void writeIndex(List<CrashLog> crashLogs) throws IOException {
        if (!indexAlreadyExists()) {
            FSDirectory dir = FSDirectory.open(new File(pathToIndex).toPath());
            IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
            IndexWriter writer = new IndexWriter(dir, config);

            int ids = 0;

            // take the set of words for each crash_log
            for (CrashLog crashLog : crashLogs) {
                ids++;
                ArrayList<String> crash_log_words = crashLog.getSetOfWords();

                FieldType myFieldType = new FieldType(TextField.TYPE_STORED);
                myFieldType.setStoreTermVectors(true);

                Document doc = new Document();
                for (String word : crash_log_words) {
                    doc.add(new Field(LuceneConstants.FIELD_CONTENT, word, myFieldType));
                }

                doc.add(new Field(LuceneConstants.FIELD_ID, crashLog.getCrashLogLocation(), myFieldType));
                writer.addDocument(doc);
                System.out.println("Added to index:" + " ID: " + ids + " crash_location: " + crashLog.getCrashLogLocation());

            }
            writer.close();
        }
    }

    private boolean indexAlreadyExists() {
        int elementCounter = 0;
        String cfe = "cfe";
        String cfs = "cfs";
        String si = "si";
        String lock = "lock";
        File[] files = this.indexDirectory.listFiles();
        assert files != null;
        for (File file : files) {
            if (file.getName().endsWith(cfe) || file.getName().endsWith(cfs) || file.getName().endsWith(si) || file.getName().endsWith(lock)) {
                elementCounter++;
            }
        }
        return elementCounter == 4;
    }

    private IndexReader getIndex() throws IOException {
        return DirectoryReader.open(FSDirectory.open(Paths.get(this.pathToIndex)));
    }

    /**
     * it returns the whole tf_idf score map. The hashmap has crash_log_path as key and termScoreMap as value
     * Each termscoreMap has a term as a key associated with its own tf_idf
     *
     * @return the whole score map
     * @throws Exception
     */
    public HashMap<String, HashMap> getScoreMap() throws Exception {
        HashMap<String, HashMap> scoreMap = new HashMap<>();
        IndexReader indexReader = getIndex();
        int numberOfDocuments = indexReader.numDocs();

        for (int i = 0; i < numberOfDocuments; i++) {
            Document doc = indexReader.document(i);
            String log_path = doc.get("id");
            Terms terms = indexReader.getTermVector(i, "word");
            TermsEnum itr = terms.iterator();
            scoreMap.put(log_path, computeTFIDFAlgorithm(indexReader, numberOfDocuments, itr));

        }
        indexReader.close();
        return scoreMap;
    }

    /**
     * iterates through all the terms in the term given vector, for the current document at the index i
     * // frequency of the given term across the whole index
     * // long totalTermFreq = indexReader.totalTermFreq(term);
     *
     * @param numberOfDocuments the total number of documents in the collection (IDF)
     */
    private HashMap<String, Double> computeTFIDFAlgorithm(IndexReader indexReader, int numberOfDocuments, TermsEnum itr) throws IOException {
        HashMap<String, Double> termScoreMap = new HashMap<>(); // contains the inspected term associated with its own tf_idf
        BytesRef term;
        while ((term = itr.next()) != null) {
            String termText = term.utf8ToString();
            // remove the terms which have only numbers of our search
//            if (!termText.matches("[0-9]+")) {
                Term termInstance = new Term("word", term);

                long termFreq = itr.totalTermFreq(); // the frequency of the term within a document
                long docFreq = indexReader.docFreq(termInstance); //   the number of documents which contain the term
                double tf_idf = getTFIDF(numberOfDocuments, termFreq, docFreq);
                termScoreMap.put(termText, tf_idf);
                //System.out.println("term: " + termText + ", termFreq = " + termFreq + ", docFreq = " + docFreq + ", tf_idf: " + tf_idf);
//            }
        }

        return termScoreMap;
    }

    public void printScoreMap(HashMap<String, HashMap> scoreMap) {
        scoreMap.forEach((crash_log_path, termScoreMap) -> {
            System.out.println("ScoreMap key: " + crash_log_path + "  {");
            termScoreMap.forEach((term, tf_idf) -> {
                System.out.println("\t\t" + term + ", tf_idf = " + tf_idf);
            });
            System.out.println("  }");
        });
    }


}
