package ch.uzh.ifi.seals.Clustering;

import ch.uzh.ifi.seals.Linking.Preprocessing.DuplicateRemoval;
import ch.uzh.ifi.seals.Linking.Preprocessing.PreprocessingFacade;
import ch.uzh.ifi.seals.Linking.Review;
import ch.uzh.ifi.seals.Parsers.PreProcessedReportParser;
import ch.uzh.ifi.seals.config.ClusteringManager;

import java.io.IOException;
import java.util.*;

/**
 * Created by LuckyP on 27.03.17.
 */
public class CrashLog {

    // After how many lines from the "java.lang" pointer, a method can be considered relevant for the bug
    private final static int STACK_TRACE_DEPTH = Integer.valueOf(ClusteringManager.getInstance().getStackTraceDepth());
    private Long PID;
    private String packageName;
    //The last element of the array represents the bottom of the stack, which is the least recent method invocation in the sequence.
    private ArrayList<String> triggerMethods;
    private String triggerClass;
    private String crashLogLocation;
    private String firstJavaStackTraceLine;
    private String shortMessage;
    private String longMessage;
    private ArrayList<String> setOfWords;
    private List<String> logLines;
    private Iterator<String> iterator;
    private String afterPreprocessing;
    private List<String> crashLogMethods;
    private String augmentedStackTraces;
    private HashMap<Review, Double> reviewDiceScoreMap;

    public CrashLog(String crashLogLocation, String packageName, List<String> logLines) throws IOException {
        this.crashLogLocation = crashLogLocation;
        this.packageName = packageName;
        this.logLines = logLines;
        this.crashLogMethods = new ArrayList<>();
        this.triggerMethods = new ArrayList<>();
        this.setOfWords = new ArrayList<>();
        this.iterator = this.logLines.iterator();
        this.reviewDiceScoreMap = new HashMap<>();
        this.parseLog();
        this.tokenizeLog();
    }


    public static int getStackTraceDepth() {
        return STACK_TRACE_DEPTH;
    }

    /**
     * This methods goes through a crash log a parses it according to some RE specified in the PatternCollector class
     */
    private void parseLog() throws IOException {
        PatternCollector patternCollector = new PatternCollector();

        patternCollector.setRegex(PatternCollector.SHORT_MESSAGE_PATTERN, this.logLines.get(1));
        if (patternCollector.getMatcher().find()) {
            this.shortMessage = (patternCollector.getMatcher().group().substring(10).trim());
        }
        patternCollector.setRegex(PatternCollector.LONG_MESSAGE_PATTERN, this.logLines.get(2));
        if (patternCollector.getMatcher().find()) {
            this.longMessage = (patternCollector.getMatcher().group().substring(10).trim());
        }


        patternCollector.setRegex(PatternCollector.FIRST_JAVA_STACK_TRACE_LINE, this.logLines.get(6));
        if (patternCollector.getMatcher().find()) {
            this.firstJavaStackTraceLine = (patternCollector.getMatcher().group().trim());
        }

        String line;
        setLinePointer(this.iterator, this.iterator.next());

        int fromTopCounter = 0;
        while (this.iterator.hasNext()) {
            line = this.iterator.next();
            while (fromTopCounter != STACK_TRACE_DEPTH) {
                fromTopCounter++;
                if (fromTopCounter == 1) {
                    this.triggerClass = line.split("\\(")[1].split("\\.")[0];
                }
                this.triggerMethods.add(line.substring(7));
                line = this.iterator.next();
            }

        }

    }

    /**
     * TODO: ADAPT THIS METHOD
     * @return
     * @throws IOException
     */
    private ArrayList<String> getCommonMethodsWithSourceCode() throws IOException {
        for (String line : this.logLines) {
            if (line.startsWith("// 	at") && line.contains(".java:")) {
                this.crashLogMethods.add(line.split("\\(")[1].split("\\.")[0]);
            }
        }
        String pText = "";
        // pText contains all methods within the stack trace, split by a white space
        for (String line : this.crashLogMethods) {
            pText += line + " ";
        }
        pText = DuplicateRemoval.removeDuplicates(pText);
        this.crashLogMethods.clear();
        this.crashLogMethods.addAll(Arrays.asList(pText.split("\\s")));


        HashMap<String, String> collection = PreProcessedReportParser.getClassesFromCSV();
        //ArrayList<String> classes = PreProcessedReportParser.getClassesFromCSV();
        ArrayList<String> augmentedStackTrace = new ArrayList<>();
        collection.forEach((pClass, additionalWords)-> {
            if (this.crashLogMethods.contains(pClass)) {
                augmentedStackTrace.add(additionalWords);
            }
        });
        return augmentedStackTrace;
    }

    /**
     * It sets the pointer to the "start-line" depending on the crash_log type
     *
     * @param iterator
     * @param line
     * @return
     */
    private void setLinePointer(Iterator<String> iterator, String line) {
        int lineCounter = 0;
        while (lineCounter++ != 7) {
            line = iterator.next();
        }

        if (containCausedBy()) {
            while (!line.contains("// Caused by:")) {
                line = iterator.next();
            }
        }
    }

    private void tokenizeLog() throws IOException {
        LuceneTokenizer luceneTokenizer = new LuceneTokenizer();
        for (String line : this.logLines) {
            luceneTokenizer.tokenizeLine(line);
        }
        //this.firstJavaStackTraceLine = luceneTokenizer.tokenizeSingleString(this.firstJavaStackTraceLine);
        this.afterPreprocessing = luceneTokenizer.tokenizeSingleString(this.firstJavaStackTraceLine) + ' ' + luceneTokenizer.tokenizeSingleString(this.triggerMethods.toString());
        this.setOfWords = luceneTokenizer.getTokenizedCrashLog();

        // set common words with source code
        ArrayList<String> commonClasses = getCommonMethodsWithSourceCode();
        String text = "";
        for (String pClass : commonClasses) {
            text += pClass + ' ';
        }

        this.augmentedStackTraces = text;

    }

    public ArrayList<String> getSetOfWords() {
        return setOfWords;
    }

    public void setSetOfWords(ArrayList<String> setOfWords) {
        this.setOfWords = setOfWords;
    }

    private boolean containCausedBy() {
        for (String logLine : this.logLines) {
            if (logLine.contains("// Caused by:")) {
                return true;
            }
        }
        return false;
    }

    public String getCrashLogLocation() {
        return crashLogLocation;
    }

    public void setCrashLogLocation(String crashLogLocation) {
        this.crashLogLocation = crashLogLocation;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public String toString() {
        return "Crash {\n" +
                "\tcrash_path = " + this.crashLogLocation + "\n" +
                "\tpackageName = " + this.packageName + "\n" +
                "\tshort = " + this.shortMessage + "\n" +
                "\tlong = " + this.longMessage + "\n" +
                "\tfirst_java_trace_line = " + this.firstJavaStackTraceLine + "\n" +
                "\ttrigger_method = " + this.triggerMethods + "\n" +
                "\ttrigger_class = " + this.triggerClass + "\n" +
                //"\tset_of_words = " + this.setOfWords + "\n" +
                "\tall_stack_trace_methods = " + this.crashLogMethods + "\n" +
                // "\tafter_pre_processing = " + this.afterPreprocessing + "\n" +
                "\taugmented_stack_traces = " + this.augmentedStackTraces //+ "\n" +
               // "\treview_Dice_ScoreMap = " + this.reviewDiceScoreMap
                + "\n}\n";
    }

    public String getFirstJavaStackTraceLine() {
        return firstJavaStackTraceLine;
    }

    public void setFirstJavaStackTraceLine(String firstJavaStackTraceLine) {
        this.firstJavaStackTraceLine = firstJavaStackTraceLine;
    }

    public Long getPID() {
        return PID;
    }

    public void setPID(Long PID) {
        this.PID = PID;
    }

    public ArrayList<String> getTriggerMethods() {
        return triggerMethods;
    }

    public void setTriggerMethods(ArrayList<String> triggerMethods) {
        this.triggerMethods = triggerMethods;
    }

    public String getTriggerClass() {
        return triggerClass;
    }

    public void setTriggerClass(String triggerClass) {
        this.triggerClass = triggerClass;
    }

    public String getShortMessage() {
        return shortMessage;
    }

    public void setShortMessage(String shortMessage) {
        this.shortMessage = shortMessage;
    }

    public String getLongMessage() {
        return longMessage;
    }

    public void setLongMessage(String longMessage) {
        this.longMessage = longMessage;
    }

    public List<String> getLogLines() {
        return logLines;
    }

    public void setLogLines(List<String> logLines) {
        this.logLines = logLines;
    }

    public Iterator<String> getIterator() {
        return iterator;
    }

    public void setIterator(Iterator<String> iterator) {
        this.iterator = iterator;
    }

    public String getAfterPreprocessing() {
        return afterPreprocessing;
    }

    public void setAfterPreprocessing(String afterPreprocessing) {
        this.afterPreprocessing = afterPreprocessing;
    }

    public List<String> getCrashLogMethods() {
        return crashLogMethods;
    }

    public void setCrashLogMethods(List<String> crashLogMethods) {
        this.crashLogMethods = crashLogMethods;
    }

    public String getAugmentedStackTraces() {
        return augmentedStackTraces;
    }

    public void setAugmentedStackTraces(String augmentedStackTraces) {
        this.augmentedStackTraces = augmentedStackTraces;
    }

    public HashMap<Review, Double> getReviewDiceScoreMap() {
        return reviewDiceScoreMap;
    }

    class Frame {
        private String signature;
        private String method;
        private String class_involved;
        private int offset;
    }
}
