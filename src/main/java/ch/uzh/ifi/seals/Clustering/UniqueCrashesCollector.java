package ch.uzh.ifi.seals.Clustering;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by LuckyP on 14.05.17.
 */

// TODO : NOT INSERT THE CRASH_LOG PATH IN THE UNIQUE_CRASHES BUT THE CRASH_LOG OBJECT
public class UniqueCrashesCollector {

    private List<CrashLog> crashLogList;
    private List<String> keys;
    private List<CrashLog> unique_crashes;
    private HashMap<String, List<CrashLog>> crashLogBucket;
    private HashMap<String, HashMap> scoreMap;
    private CrashLogFinder crashLogFinder;
    private double maxSimilarity = 0;

    public UniqueCrashesCollector(List<CrashLog> crashLogList, HashMap<String, HashMap> scoreMap) {
        this.keys = new ArrayList<>();
        this.keys.addAll(scoreMap.keySet());
        this.scoreMap = scoreMap;
        this.unique_crashes = new ArrayList<>();
        this.crashLogList = crashLogList;
        this.crashLogFinder = new CrashLogFinder(this.crashLogList);
        this.crashLogBucket = new HashMap<>();
    }

    public void fillUniqueCrashesCollector() throws InterruptedException {
        int numberOfCrashes = this.scoreMap.keySet().size();
        for (int i = 0; i < numberOfCrashes; i++) {
            if (this.unique_crashes.size() == 0) { // insert a first crash_log that acts as reference point for the next ones
                String first_crash_log = this.keys.get(i);
                CrashLog crashLog = crashLogFinder.getCrashLog(first_crash_log);
                this.unique_crashes.add(crashLog);
            } else {
                String inspectedCrashLog = keys.get(i);
                CrashLog crashLog = crashLogFinder.getCrashLog(inspectedCrashLog);
                HashMap<String, Double> inspectedMap = this.scoreMap.get(inspectedCrashLog);
                int counter = 0;
                for (CrashLog listedCrashLog : this.unique_crashes) {
                    String listed_crash_log_path = listedCrashLog.getCrashLogLocation();
                    counter += updateCounter(inspectedMap, listed_crash_log_path);
                }
                // there is no crash which has a cosine similarity greater than 0.80. This means that there is no crash_log
                // inside the "unique_crashes" list which is the same as the inspected crash_log
                if (counter == this.unique_crashes.size()) {
                    this.unique_crashes.add(crashLog);
                }
            }
        }
        createCrashLogBucket();
    }

    private void createCrashLogBucket() throws InterruptedException {
        for (CrashLog crashLog : this.unique_crashes) {
            List<CrashLog> crashLogs = new ArrayList<>();
            crashLogs.add(crashLog);
            this.crashLogBucket.put(crashLog.getCrashLogLocation(), crashLogs);
        }

        fillCrashLogBucket();
    }

    /**
     * It classifies the each crash_log according to their cosine similarity in the right crash_log list, which is defined
     * in the crash_log bucket. After that the bucket is set: Each crash_log is grouped is its correct category
     * @throws InterruptedException
     */
    private void fillCrashLogBucket() throws InterruptedException {
        for (CrashLog crashLog : this.crashLogList) {
            this.maxSimilarity = 0;
            final CrashLog[] key = new CrashLog[1];
            if (!this.unique_crashes.contains(crashLog)) {
                //System.err.println("INSPECTED CRASH LOG " + crashLog.getCrashLogLocation());
                HashMap<String, Double> inspectedMap = this.scoreMap.get(crashLog.getCrashLogLocation());
                this.crashLogBucket.forEach((path, crash_logs) -> {
                    HashMap<String, Double> keyMap = this.scoreMap.get(path); // it contains the "term-tfidf vector" of the key of the bucketCrashLog

                    // it compares now all the keyMaps (that are already in the bucket) with the map which is going to be inspected.
                    // the "keyMap" which has the greatest cosine similarity with the inspected map will be the "parent" of the inspected map
                    // this means that the inspected map will be classified in the crash_log list of the resp. key
                    CosineSimilarityCalculator cosineSimilarityCalculator = new CosineSimilarityCalculator(inspectedMap, keyMap);
                    System.out.print(crashLog.getCrashLogLocation() + " VS " + path + "   :    " + cosineSimilarityCalculator.getCosineSimilarity());
                    System.out.println();
                    if (computeMaxSimilarity(crash_logs.get(0), cosineSimilarityCalculator.getCosineSimilarity()) != null) { // key crash_log is always in the first position of the crash_log list
                        key[0] = crash_logs.get(0);
                    }

                });
                this.crashLogBucket.get(key[0].getCrashLogLocation()).add(crashLog);
                System.out.println("\n\n\n");
            }
        }
    }

    private CrashLog computeMaxSimilarity(CrashLog keyCrashLog, double coseneSimilarity) {
        if (this.maxSimilarity < coseneSimilarity) {
            this.maxSimilarity = coseneSimilarity;
            return keyCrashLog;
        }
        return null;
    }

    public HashMap<String, List<CrashLog>> getCrashLogBucket() {
        return crashLogBucket;
    }

    public void printCrashLogBucket() {
        final int[] setCounter = {1};
        this.crashLogBucket.forEach( (crash_log_location, crash_log_list) -> {
            System.out.println("Set number: " + setCounter[0]++);
            crash_log_list.forEach( crashLog ->  {
                System.out.println(crashLog.getCrashLogLocation());
            });
            System.out.println("\n");
        } );

    }

    /**
     * @param inspectedMap   the map which is going to be investigated. If it contains a unique crash_log is going to be added in the list
     * @param listedDocument the name of a crash_log which is already in the list of <code>unique_crashes</code>
     * @return
     */
    private int updateCounter(HashMap<String, Double> inspectedMap, String listedDocument) {
        HashMap<String, Double> listedMap = scoreMap.get(listedDocument);
        Oracle oracle = new Oracle(new CosineSimilarityCalculator(inspectedMap, listedMap));
        if (!oracle.areCrashLogsEquals()) { // if the inspected CrashLog is not "the same" as the other ones which are already in the list
            return 1;
        }
        return 0;
    }

    public List<CrashLog> getUnique_crashes() {
        return unique_crashes;
    }
}
