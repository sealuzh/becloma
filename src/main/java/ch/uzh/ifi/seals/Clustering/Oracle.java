package ch.uzh.ifi.seals.Clustering;


import ch.uzh.ifi.seals.config.ClusteringManager;

/**
 * Created by LuckyP on 08.05.17.
 */
public class Oracle {

    // this value represents the tolerance for evaluating the similarity between two crash_logs
    // if the calculated cosineSimilarity is greater than the given tolerance, the two crash_logs are considered as the same.
    // (which means that they actually describe the same bug)
    private static final double SIMILARITY_TOLERANCE = Double.valueOf(ClusteringManager.getInstance().getSimilarityTolerance()); // tested for com.ringdroid
    private CosineSimilarityCalculator cosineSimilarityCalculator;

    public Oracle(CosineSimilarityCalculator cosineSimilarityCalculator) {
        this.cosineSimilarityCalculator = cosineSimilarityCalculator;
    }

    public boolean areCrashLogsEquals() {
        return this.cosineSimilarityCalculator.getCosineSimilarity() > SIMILARITY_TOLERANCE;
    }
}
