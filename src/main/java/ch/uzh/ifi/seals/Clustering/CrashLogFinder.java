package ch.uzh.ifi.seals.Clustering;

import java.util.List;

/**
 * Created by LuckyP on 15.05.17.
 */
public class CrashLogFinder {

    private List<CrashLog> crashLogList;

    public CrashLogFinder(List<CrashLog> crashLogList) {
        this.crashLogList = crashLogList;
    }

    public CrashLog getCrashLog(String crash_log_unique_path) {
        for (CrashLog crashLog : this.crashLogList) {
            if (crash_log_unique_path.equals(crashLog.getCrashLogLocation())) {
                return crashLog;
            }
        }
        return null;
    }
}
