package ch.uzh.ifi.seals.Clustering;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by LuckyP on 01.04.17.
 */
public class PatternCollector {

    private Matcher matcher;
    private Pattern pattern;
    public static final String PID_PATTERN = "pid\\s\\d+";
    public static final String SHORT_MESSAGE_PATTERN ="\\bShort\\sMsg\\:\\s.*";
    public static final String LONG_MESSAGE_PATTERN ="\\bLong\\sMsg\\:\\s.*";
    public static final String FIRST_JAVA_STACK_TRACE_LINE = "\\s.*";
    public static final String TRIGGER_METHOD_PATTERN = "";

    /**
     * Set the <code>Matcher</code> and the <code>Pattern</code> instance with the regex to look at
     * @param pattern
     * @param line
     */
    public void setRegex(String pattern, String line) {
        this.pattern = Pattern.compile(pattern);
        this.matcher = this.pattern.matcher(line);
    }

    public Matcher getMatcher() {
        return this.matcher;
    }


}
