package ch.uzh.ifi.seals.Clustering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class CrashLogCollector initializes a Crash_Log HashMap which collect all the crash_logs and classifies them
 * in according to their packageName
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 09.05.2017
 */
public class CrashLogCollector {

    private List<CrashLog> crashLogList;
    private Map<String, List<CrashLog>> crashLogsMap;

    public CrashLogCollector() {
        this.crashLogList = CrashLogParser.getCrashLogs();
        this.crashLogsMap = new HashMap<>();
        this.createCrashLogLabels();
        this.differentiateCrashLogs();
    }

    public Map<String, List<CrashLog>> getCrashLogsMap() {
        return crashLogsMap;
    }

    /**
     * this methods initializes the crash log map with the package names contained in the crash_log_container
     */
    private void createCrashLogLabels() {
        for (CrashLog crashLog : this.crashLogList) {
            if (!crashLogsMap.keySet().contains(crashLog.getPackageName())) {
                crashLogsMap.put(crashLog.getPackageName(), new ArrayList<>());
            }
        }
    }

    /**
     * Depending on their package name, the crash_logs are added into the HashMap
     */
    public void differentiateCrashLogs() {
        for (CrashLog crashLog : this.crashLogList) {
            String packageName = crashLog.getPackageName();
            crashLogsMap.get(packageName).add(crashLog);
        }

    }
}
