package ch.uzh.ifi.seals.Clustering;

import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import sun.security.x509.AttributeNameEnumeration;

import java.text.NumberFormat;
import java.util.*;

/**
 * Created by LuckyP on 13.05.17.
 */
public class CosineSimilarityCalculator {

    private List<Double> v1;
    private List<Double> v2;
    private String document1;
    private String document2;
    private HashMap<String, Double> termScoreMap1;
    private HashMap<String, Double> termScoreMap2;
    private NumberFormat defaultFormat;

    public CosineSimilarityCalculator(String document1, String document2, HashMap<String, Double> termScoreMap1, HashMap<String, Double> termScoreMap2) {
        this.defaultFormat = NumberFormat.getPercentInstance();
        this.defaultFormat.setMinimumFractionDigits(1);
        this.termScoreMap1 = termScoreMap1;
        this.termScoreMap2 = termScoreMap2;
        this.v1 = new ArrayList<>();
        this.v2 = new ArrayList<>();
        this.document1 = document1;
        this.document2 = document2;
        this.normalizeScoreMaps();
    }

    public CosineSimilarityCalculator(HashMap<String, Double> termScoreMap1, HashMap<String, Double> termScoreMap2) {
        this.defaultFormat = NumberFormat.getPercentInstance();
        this.defaultFormat.setMinimumFractionDigits(1);
        this.termScoreMap1 = termScoreMap1;
        this.termScoreMap2 = termScoreMap2;
        this.v1 = new ArrayList<>();
        this.v2 = new ArrayList<>();
        this.normalizeScoreMaps();
    }




    private void normalizeScoreMaps() {
        this.termScoreMap1.forEach((term, tf_idf) -> {
            if (!this.termScoreMap2.containsKey(term)) {
                this.termScoreMap2.put(term, 0d);
            }
        });
        this.termScoreMap2.forEach((term, tf_idf) -> {
            if (!this.termScoreMap1.containsKey(term)) {
                this.termScoreMap1.put(term, 0d);
            }
        });
        assert (this.termScoreMap1.keySet().size() == this.termScoreMap2.keySet().size());
        this.termScoreMap1.forEach((term, tf_idf) -> this.v1.add(tf_idf));
        this.termScoreMap2.forEach((term, tf_idf) -> this.v2.add(tf_idf));
    }


    private double getDotProduct(List<Double> v1, List<Double> v2) {
        double dotSum = 0;
        for (int i = 0; i < v1.size(); i++) {
            dotSum += v1.get(i) * v2.get(i);
        }
        return dotSum;
    }

    private double getNorm(List<Double> v) {
        double norm = 0;
        for (Double num : v) {
            norm += Math.pow(num, 2);
        }
        return Math.sqrt(norm);
    }


    public String getCosineSimilarityPercentage() {
        return this.document1 + " and " + this.document2 + " are " +
                this.defaultFormat.format(getDotProduct(v1, v2) / ((getNorm(v1) * getNorm(v2)))) + " similar!\n";
    }

    public double getCosineSimilarity() {
        return getDotProduct(v1, v2) / ((getNorm(v1) * getNorm(v2)));
    }


}
