package ch.uzh.ifi.seals.Clustering;


import ch.uzh.ifi.seals.config.Regex;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by LuckyP on 04.05.17.
 */
public class LuceneTokenizer {

    private ArrayList<String> tokenizedCrashLog;

    public LuceneTokenizer() {
        this.tokenizedCrashLog = new ArrayList<>();
    }

    private static TokenStream createTokenStream(String input) {
        MyAnalyser analyser = new MyAnalyser();
        return analyser.tokenStream("field", new StringReader(input));
    }

    public ArrayList<String> getTokenizedCrashLog() {
        return tokenizedCrashLog;
    }

    /**
     * The given string input gets tokenized
     *
     * @param input
     * @throws IOException
     */
    public void tokenizeLine(String input) throws IOException {
        TokenStream stream = createTokenStream(input);
        CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);
        try {
            stream.reset();

            // print all tokens until stream is exhausted
            while (stream.incrementToken()) {
                String term = termAtt.toString();
                if (term.contains(".")) {
                    String[] word_split = term.split("\\.");
                    for (String word : word_split) {
                        for (String cameCase : word.split(getCamelCaseRegex())) {
                            this.tokenizedCrashLog.add(cameCase.toLowerCase().trim());
                        }
                    }

                } else {
                    this.tokenizedCrashLog.add(term.toLowerCase().trim());
                }

            }

            stream.end();
        } finally {
            stream.close();
        }
    }

    public String tokenizeSingleString(String input) throws IOException {
        String toReturn = "";
        TokenStream stream = createTokenStream(input);
        CharTermAttribute termAtt = stream.addAttribute(CharTermAttribute.class);
        try {
            stream.reset();
            while (stream.incrementToken()) {
                String term = termAtt.toString();
                if (term.contains(".")) {
                    String[] word_split = term.split("\\.");
                    for (String word : word_split) {
                        for (String cameCase : word.split(getCamelCaseRegex())) {
                            toReturn += cameCase + " ";
                            ;
                        }
                    }

                } else {
                    toReturn += term.toLowerCase().trim() + " ";
                }

            }

            stream.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            stream.close();
        }
        return toReturn;
    }

    private String getCamelCaseRegex() {
        return "(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])";
    }
}
