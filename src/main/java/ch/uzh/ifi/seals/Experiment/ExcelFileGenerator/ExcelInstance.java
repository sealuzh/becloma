package ch.uzh.ifi.seals.Experiment.ExcelFileGenerator;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * This interface is used by these classes which have to write some data into an excel file.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 04.04.2017
 */
public interface ExcelInstance {

    /**
     * A new Excel Instance (= Excel sheet) is created
     */
    void createExcelInstance();

    /**
     * Defines the title of the Excel sheet
     */
    void setExcelSheetTitle();

    /**
     * Write the excel outputStream into the excel file
     */
    void writeResultsOnExcelSheet(FileOutputStream outputStream) throws IOException;


}
