package ch.uzh.ifi.seals.Experiment.ExcelFileGenerator;


import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;

/**
 * The Class ApachePOIExcel implements some write/read operations on an excel file using the Apache POI library. This
 * class is used for writing the statistics provided by different testing sessions.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 13.03.2017
 */
public class ApachePOIExcel {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private XSSFRow firstRow;
    private XSSFRow secondRow;
    private String generationNr;


    public ApachePOIExcel(String generationNr) {
        setEnvironment(generationNr);
    }

    /**
     * It sets the environment for this class.
     * This method is invoked each time a new object of this class is created and instantiated.
     */
    private void setEnvironment(String generationNr) {
        this.generationNr = generationNr;
        createExcelSheet();
    }

    public HashMap<String, Integer> getOnGoingStatistics(InputStream inputStream) throws IOException {
        HashMap<String, Integer> values = new HashMap<>();
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);
        for (Row nextRow : sheet) {
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            String packageName = "";
            int nrCrash = -1;
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.toString().contains(".")) {
                    if (cell.getColumnIndex() == 0) {
                        packageName = cell.toString();
                    }
                    if (cell.getColumnIndex() == 1) {
                        nrCrash = Double.valueOf(cell.toString()).intValue();
                    }
                }
            }
            if (!packageName.isEmpty()) {
                values.put(packageName, nrCrash);
            }
        }

        return values;
    }

    public void write(OutputStream outputStream) throws IOException {
        this.workbook.write(outputStream);
    }

    public void close() throws IOException {
        this.workbook.close();
    }

    public void writeTitle(String title) {
        this.firstRow.createCell(0).setCellValue(title);
        System.out.println("Title has been created ");
    }

    public void writeOnExcelFile(String packageName, int numberOfCrashes) {
        int rowNum = this.sheet.getPhysicalNumberOfRows();
        XSSFRow nextRow = this.sheet.createRow(rowNum++);
        nextRow.createCell(0).setCellValue(packageName);
        nextRow.createCell(1).setCellValue(numberOfCrashes);
        System.out.println("Statistics for " + packageName + " have been written");
    }

    private void createExcelSheet() {
        this.workbook = new XSSFWorkbook();
        if (generationNr.contains("_")) {
            this.sheet = workbook.createSheet("Experiment_" + generationNr);
        } else {
            this.sheet = workbook.createSheet("TestGeneration_" + generationNr);
        }

        this.firstRow = this.sheet.createRow(0);
        this.secondRow = this.sheet.createRow(1);
        this.secondRow.createCell(0).setCellValue("Package Name");
        this.secondRow.createCell(1).setCellValue("Number of crash");
        sheet.setColumnWidth(0, 8500);
        sheet.setColumnWidth(1, 4000);
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);//set it to bold
        this.secondRow.getCell(0).setCellStyle(style);
        this.secondRow.getCell(1).setCellStyle(style);
    }
}

