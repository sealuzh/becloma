package ch.uzh.ifi.seals.Experiment;

import ch.uzh.ifi.seals.Experiment.Statistics.FinalCalculator;
import ch.uzh.ifi.seals.Experiment.Statistics.OnGoingCalculator;
import ch.uzh.ifi.seals.Main.Main;
import ch.uzh.ifi.seals.Tester.AppTester;
import ch.uzh.ifi.seals.config.ConfigurationManager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static ch.uzh.ifi.seals.Main.Main.HAS_TIMER;

/**
 * This class manages the whole experiment.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 03.03.2017
 */
public class ExperimentLauncher {

    private static int firstExperimentIndex;
    private static int lastExperimentIndex;
    private static int numberOfIteration;
    private static boolean isFirstExperimentIndex;
    private AppTester appTester;
    private int iterationNr = 0;
    private Logger logger;

    public ExperimentLauncher() throws InterruptedException, IOException {
        setEnvironment();
    }

    /**
     * Defines the index ranges for the experiment
     *
     * @param currentTestGenerationIndex (current TestGeneration index)
     */
    public static void defineExperimentIndexRange(int currentTestGenerationIndex) {
        if (isFirstExperimentIndex) {
            firstExperimentIndex = currentTestGenerationIndex;
            lastExperimentIndex = currentTestGenerationIndex + numberOfIteration - 1;
            isFirstExperimentIndex = false;
        }
    }

    /**
     * Setters & Getters
     */
    public static int getFirstExperimentIndex() {
        return firstExperimentIndex;
    }

    public static int getLastExperimentIndex() {
        return lastExperimentIndex;
    }

    /**
     * It sets the environment for this class.
     * This method is invoked each time a new object of this class is created and instantiated.
     *
     * @throws InterruptedException
     */
    private void setEnvironment() throws InterruptedException, IOException {
        ConfigurationManager config = ConfigurationManager.getInstance();
        this.appTester = new AppTester();
        this.logger = Logger.getLogger(getClass().getName());
        numberOfIteration = Integer.valueOf(config.getNumberOfIterations());
    }

    /**
     * Performs the whole experiment. It tests <numberOfIteration> times the whole dataset in according to the
     * properties specified in the config File.
     *
     * @throws InterruptedException
     * @throws IOException
     */
    public void performExperiment() throws Exception {
        isFirstExperimentIndex = true;
        for (int i = 0; i < numberOfIteration; i++) {
            setIterationNr(getIterationNr() + 1);
            setTimer();
            if (i == 0) {
                logger.info("--------------------------------------------" +
                        "ITERATION NUMBER: " + (i + 1) + "-------------------------------------------");
            } else {
                logger.info("\n\n\n\n\n\n\n--------------------------------------------" +
                        "ITERATION NUMBER: " + (i + 1) + "-------------------------------------------");
            }
            computeIterationStep();
        }
        computeExperimentStep();
    }

    /**
     * At each iteration the following steps must be completed:
     * 1. Test the whole DataSet
     * 2. Extract for the current TestGeneration possibles crashes
     * 3. Compute "On-Going" statistics
     *
     * @throws InterruptedException
     * @throws IOException
     */
    private void computeIterationStep() throws Exception {
        CrashExtractor crashExtractor = new CrashExtractor();
        appTester.testAllApp();
        crashExtractor.extractAllCrashForTestGeneration();
        TimeUnit.SECONDS.sleep(2);
        OnGoingCalculator onGoingCalculator = new OnGoingCalculator();
        onGoingCalculator.computeOnGoingStatistics();
        Main.initializeDevices();
    }

    /**
     * At the end of the experiment the following steps must be completed:
     * 1. Extract all crashes of all the TestGenerations involved in the experiment
     * 2. Compute "Final"- statistics
     *
     * @throws InterruptedException
     * @throws IOException
     */
    private void computeExperimentStep() throws InterruptedException, IOException {
        TimeUnit.SECONDS.sleep(5);
        CrashExtractor crashExtractor = new CrashExtractor();
        FinalCalculator finalCalculator = new FinalCalculator();
        finalCalculator.computeExperimentStatistics();
        logger.info("Experiment Statistics have been updated!");
    }

    /**
     * In According to the config File a timer can be set for a better overview during the testing experiment
     */
    private void setTimer() {
        if (HAS_TIMER) {
            Main.getChronometer().startChronometer();
            Main.getChronometer().updateIterationLabel(getIterationNr());
        }
    }

    private int getIterationNr() {
        return iterationNr;
    }

    private void setIterationNr(int iterationNr) {
        this.iterationNr = iterationNr;
    }


}
