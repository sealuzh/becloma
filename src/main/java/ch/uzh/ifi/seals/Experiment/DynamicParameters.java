package ch.uzh.ifi.seals.Experiment;

import ch.uzh.ifi.seals.config.ConfigurationManager;

import java.util.Random;

/**
 * The Class DynamicParameters builds the new parameters for monkey if there is more than one iteration
 * in the experiment. The first iteration takes by default the inserted parameters in the <config.properties>
 * file; after that, the parameters grow in accord with the methods defined in this class.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 02.03.2017
 */
public abstract class DynamicParameters {

    private static final int PERCENTAGE_TOUCH_EVENTS = Integer.valueOf(ConfigurationManager.getInstance().getPercentageTouchEvent());
    private static final int PERCENTAGE_SYSTEM_EVENTS = Integer.valueOf(ConfigurationManager.getInstance().getPercentageSystemEvent());
    private static final int PERCENTAGE_MOTION_EVENTS = Integer.valueOf(ConfigurationManager.getInstance().getPercentageMotionEvent());
    public static int iterationValue;

    /**
     * builds the new percentage
     *
     * @param iterationValue (number of iteration in the experiment)
     * @return new percentage
     */
    private static int getNewPercentage(int iterationValue) {
        Random random = new Random();
        return Math.min((random.nextInt(getIncrementalValue()) + 1) * iterationValue, 33);
    }

    /**
     * builds and return the new percentage of touch events which have to be used for the next testing session
     *
     * @param iterationValue (number of iteration in the experiment)
     * @return a new percentage (int)
     */
    public static int getNewPercentageTouchEvents(int iterationValue) {
        if (iterationValue == 0) {
            return PERCENTAGE_TOUCH_EVENTS;
        }
        return getNewPercentage(iterationValue);
    }

    /**
     * builds and return the new percentage of system events which have to be used for the next testing session
     *
     * @param iterationValue (number of iteration in the experiment)
     * @return a new percentage (int)
     */
    public static int getNewPercentageSystemEvents(int iterationValue) {
        if (iterationValue == 0) {
            return PERCENTAGE_SYSTEM_EVENTS;
        }
        return getNewPercentage(iterationValue);
    }

    /**
     * builds and return the new percentage of motion events which have to be used for the next testing session
     *
     * @param iterationValue (number of iteration in the experiment)
     * @return a new percentage (int)
     */
    public static int getNewPercentageMotionEvents(int iterationValue) {
        if (iterationValue == 0) {
            return PERCENTAGE_MOTION_EVENTS;
        }
        return getNewPercentage(iterationValue);
    }

    /**
     * defines and returns a value for increment the new percentages
     *
     * @return integer
     */
    private static int getIncrementalValue() {
        int value = 33 / Integer.valueOf(ConfigurationManager.getInstance().getNumberOfIterations());
        if (value == 0) { // if the number of iterations is grater than 33
            return 1;
        }
        return value;
    }
}
