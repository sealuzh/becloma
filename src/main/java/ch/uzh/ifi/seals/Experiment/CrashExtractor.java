package ch.uzh.ifi.seals.Experiment;

import ch.uzh.ifi.seals.Tester.AppTester;
import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import org.apache.commons.io.filefilter.DirectoryFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ch.uzh.ifi.seals.Main.Main.*;

/**
 * The CrashExtractor class creates the ALL_CRASHES directories, extract the accumulated crashes
 * for each app in every TestGeneration and for the whole Experiment.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 08.03.2017
 */
public class CrashExtractor {

    private ConfigurationManager config;
    private Logger logger;
    private CommandExecutor commandExecutor;
    private AppTester appTester;
    private HashMap<File, String> crashCollector;
    private File[] reportsFiles;
    private int firstExIndex;
    private int lastExIndex;

    public CrashExtractor() throws InterruptedException, IOException {
        setEnvironment();
    }

    /**
     * It sets the environment for this class.
     * This method is invoked each time a new object of this class is created and instantiated.
     *
     * @throws InterruptedException
     */
    private void setEnvironment() throws InterruptedException, IOException {
        this.config = ConfigurationManager.getInstance();
        this.commandExecutor = new CommandExecutor();
        this.appTester = new AppTester();
        this.logger = Logger.getLogger(getClass().getName());
        this.reportsFiles = new File(REPORT_DIRECTORY).listFiles();
        this.crashCollector = new HashMap<>();
        this.logger = Logger.getLogger(getClass().getName());
    }

    /**
     * It scans the ( Monkey || Sapienz ) Report folders and fills the HashMap @crashCollector with a set of path of
     * packages, that have a crash log stored inside their own folder
     */
    private void fillCrashCollector() {
        for (File TestGeneration_ : this.reportsFiles) {
            if (TestGeneration_.toString().contains("TestGeneration") && TestGeneration_.isDirectory()) {
                scanTestGenerationFolder(TestGeneration_);
            }
        }
    }

    /**
     * It scans the TestGeneration folders and updates at each iteration the HashMap @crashCollector
     *
     * @param TestGeneration_ (the current TestGeneration folder)
     */
    private void scanTestGenerationFolder(File TestGeneration_) {
        if (TestGeneration_.exists()) {
            for (File packageFolder : TestGeneration_.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY)) {
                if (!packageFolder.getName().startsWith("ALL_CRASHES") || !packageFolder.isDirectory()) {
                    updateCrashCollector(TestGeneration_, packageFolder);
                }
            }
        } else {
            logger.log(Level.SEVERE, TestGeneration_.getAbsolutePath() + " does not exist!");
        }
    }

    /**
     * Updates the HashMap @crashCollector if there is a crash log stored inside the packageFolder. If yes, the hashMap
     * gets updated with:
     *
     * @param TestGeneration_ (the current TestGeneration folder; parent directory of the packageFolder)
     * @param packageFolder   (the current packageFolder, which has to be investigated)
     */
    private void updateCrashCollector(File TestGeneration_, File packageFolder) {
        if (IS_MONKEY) {
            int nrOfCrashes = commandExecutor.countNrOfMonkeyCrashFilesInsideAFolder(packageFolder.getAbsolutePath());
            if (nrOfCrashes > 0) {  // A crash has been detected in the packageFolder
                this.crashCollector.put(packageFolder, TestGeneration_.getAbsolutePath());
            }
        }
        if (IS_SAPIENZ) {
            for (File _output : packageFolder.listFiles()) {
                File crashlogFolder = new File(_output.getAbsolutePath() + '/' + _output.getName().split("\\.apk")[0] + ".apk" + "_output" + "/crashes");
                if (crashlogFolder.exists()) {
                    int numberOfCrashes = commandExecutor.countSapienzCrashLogs(crashlogFolder.getAbsolutePath());
                    if (numberOfCrashes > 0) { // the crash folder in the output folder of sapienz has at least one crash log
                        logger.info(numberOfCrashes + " crashes has been detected in " + crashlogFolder.getAbsolutePath());
                        this.crashCollector.put(_output, TestGeneration_.getAbsolutePath());
                    }
                } else {
                    logger.log(Level.SEVERE, crashlogFolder.getAbsolutePath() + " does not exist!");
                }

            }


        }
    }

    /**
     * Extracts all the crash files that have been generated during the testing phase in Each TestGeneration Folder.
     * It fills the HashMap @crashCollector with the paths of the _packages that have a crash log stored in their own folder
     * and for each TestGeneration creates a folder called ALL_CRASHES.
     * It scans @crashCollector and for each file (=Directory Path) it copies the whole packageFolder into the ALL_CRASHES dir
     */
    public void extractAllCrashForTestGeneration() {
        fillCrashCollector();
        if (this.crashCollector.keySet().isEmpty()) { // there are no crashes inside it
            logger.info("================No Crash Log has been detected!================");
            return;
        }
        for (File packageFolder : this.crashCollector.keySet()) {
            String sourceFolder = "";
            if (IS_SAPIENZ) {
                sourceFolder = packageFolder.getAbsolutePath();
            } else if (IS_MONKEY) { //  Users/LuckyP/Desktop/UZH/BA/BA_PROJ/Reports/MonkeyReports/TestGeneration_1/at.it.reader
                sourceFolder = packageFolder.getAbsolutePath();
            }
            String testGenerationPath = this.crashCollector.get(packageFolder);

            String ALL_CRASHES_PATH = testGenerationPath + "/ALL_CRASHES/";
            File ALL_CRASHES_DIR = new File(ALL_CRASHES_PATH);
            if (!ALL_CRASHES_DIR.exists()) {
                ALL_CRASHES_DIR.mkdir();
                logger.info(ALL_CRASHES_DIR.toString() + " has been created!");
            }
            File destinationFolder = new File(testGenerationPath + "/ALL_CRASHES/" + packageFolder.getName().split("_")[0]);
            if (!destinationFolder.exists()) {
                destinationFolder.mkdir();
            }

            commandExecutor.executeCommandWithoutTerminalView("cp -R " + sourceFolder + " " + destinationFolder.getAbsolutePath());
            logger.info("====================CRASH LOG COPIED IN FOLDER ALL_CRASHES!===================");
            //Eliminate the folder .apk in the ALL_CRASHES folder
//            if (IS_SAPIENZ) {
//                elimateAPKFolderForSapienz(ALL_CRASHES_DIR);
//            }
        }

    }

    /**
     * Eliminates the empty folders that have been generated in the ALL_CRASHES_DIR for Sapienz
     *
     * @param allCrashDir (ALL_CRASHES_DIR of the current TestGeneraiton)
     */
    private void elimateAPKFolderForSapienz(File allCrashDir) {
        if (allCrashDir.exists()) {
            ArrayList<File> crashes = new ArrayList<>(Arrays.asList(allCrashDir.listFiles()));
            for (File crash : crashes) {
                if (!crash.getName().contains("output")) {
                    crash.delete();
                }
            }
        }
    }


    /**
     * Creates the EXPERIMENT directory and copies inside it all the ALL_CRASHES directories
     * of all TestGeneration involved in the experiment.
     *
     * @throws InterruptedException
     */
    public void extractAllCrashForExperiment() throws InterruptedException, IOException {
        this.firstExIndex = ExperimentLauncher.getFirstExperimentIndex();
        this.lastExIndex = ExperimentLauncher.getLastExperimentIndex();
        final File EXPERIMENT_DIR = new File(REPORT_DIRECTORY + "/Experiment_" + this.firstExIndex + "_" + this.lastExIndex);
        if (!EXPERIMENT_DIR.exists()) {
            EXPERIMENT_DIR.mkdir();
        }
        for (File testGeneration : this.reportsFiles) {
            if (!testGeneration.isDirectory() || !testGeneration.getName().contains("TestGeneration")) {
                continue;
            }
            int testGenerationIndex = Integer.valueOf(testGeneration.getName().split("_")[1]);
            if (testGenerationIndex >= this.firstExIndex && testGenerationIndex <= this.lastExIndex) {
                logger.log(Level.SEVERE, testGeneration.toString());
                extractCrashesFromTestGeneration(EXPERIMENT_DIR, testGeneration);
            }
        }


    }

    /**
     * For the given testGeneration, if there is at least one crash stored in the ALL_CRASHES directory it will be copied
     * into the EXPERIMENT_DIR
     *
     * @param EXPERIMENT_DIR (Folder, where all crashes of all the TestGeneration involved in the experiment have to be stored)
     * @param testGeneration (The current TestGeneration, which has to be investigated)
     */
    private void extractCrashesFromTestGeneration(File EXPERIMENT_DIR, File testGeneration) {
        File ALL_CRASHES_DIR = new File(testGeneration + "/ALL_CRASHES");
        if (ALL_CRASHES_DIR.exists()) { // there is at least one crash for this TestGeneration
            File testGenInExpDir = new File(EXPERIMENT_DIR.getAbsolutePath() + "/" + testGeneration.getName());
            if (!testGenInExpDir.exists()) {
                testGenInExpDir.mkdir();
                commandExecutor.executeCommandWithoutTerminalView("cp -r " + ALL_CRASHES_DIR.getAbsolutePath() +
                        " " + testGenInExpDir.getAbsolutePath());
                logger.info("---------------" + " CRASHES IN " + testGeneration.getName() + " extracted!");
            }


        }
    }


}
