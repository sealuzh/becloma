package ch.uzh.ifi.seals.Experiment.Statistics;

import ch.uzh.ifi.seals.Experiment.ExcelFileGenerator.ApachePOIExcel;
import ch.uzh.ifi.seals.Experiment.ExcelFileGenerator.ExcelInstance;
import ch.uzh.ifi.seals.Experiment.ExperimentLauncher;
import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.ConfigurationManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ch.uzh.ifi.seals.Main.Main.*;

/**
 * The OnGoingCalculator class computes and writes the "On-Going" statistics during the
 * testing phase into an excel file, which is stored into the current TestGeneration Folder
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 03.04.2017
 */
public class OnGoingCalculator implements ExcelInstance {

    private Logger logger;
    private ConfigurationManager config;
    private CommandExecutor commandExecutor;
    private ApachePOIExcel apachePOIExcel;
    private ArrayList<String> filters;
    private int currentTestGenerationIndex;

    public OnGoingCalculator() throws InterruptedException, IOException {
        setEnvironment();
    }

    /**
     * It sets the environment for this class.
     * This method is invoked each time a new object of this class is created and instantiated.
     *
     * @throws InterruptedException
     */
    private void setEnvironment() throws InterruptedException, IOException {
        this.config = ConfigurationManager.getInstance();
        this.commandExecutor = new CommandExecutor();
        this.logger = Logger.getLogger(getClass().getName());
    }

    /**
     * Computes the statistics for each test generation during the experiment
     *
     * @throws IOException
     */
    public void computeOnGoingStatistics() throws IOException, InterruptedException {
        this.filters = new ArrayList<>();
        this.currentTestGenerationIndex = getCurrentTestGenerationFolderIndex(new File(REPORT_DIRECTORY));
        final File ALL_CRASHES_DIR = new File(REPORT_DIRECTORY + "/TestGeneration_" + this.currentTestGenerationIndex + "/ALL_CRASHES/");
        final String CURRENT_EXCEL_FILE = ALL_CRASHES_DIR.getParent() + "/TestGeneration_" + this.currentTestGenerationIndex + ".xlsx";
        final FileOutputStream OUTPUT_STREAM = new FileOutputStream(CURRENT_EXCEL_FILE);

        System.err.println("\n\n\n\n\n\n\n\n======================= Compute ingoing statistics for TestGeneration_" + this.currentTestGenerationIndex + " =======================");

        createExcelInstance();
        ExperimentLauncher.defineExperimentIndexRange(this.currentTestGenerationIndex);
        setExcelSheetTitle();

        computeAppWithCrashes(ALL_CRASHES_DIR);
        computeAppsWithNoCrashes(ALL_CRASHES_DIR);
        writeResultsOnExcelSheet(OUTPUT_STREAM);
    }

    /**
     * It iterates through the ALL_CRASHES_DIR in the current TestGeneration Folder and writes
     * into the excel file the corresponding package_name associated with its number of crashes
     *
     * @param ALL_CRASHES_DIR (A Folder in the current TestGeneration Folder, which contains all the crash_log files found during the testing)
     */
    private void computeAppWithCrashes(final File ALL_CRASHES_DIR) {
        // if there is at least one crash inside ALL_CRASH_DIR
        if (ALL_CRASHES_DIR.exists()) {
            ArrayList<File> packagesFolders = new ArrayList<>(Arrays.asList(ALL_CRASHES_DIR.listFiles()));
            for (File packageFolder : packagesFolders) {
                if (IS_MONKEY) {
                    this.filters.add(packageFolder.getName());
                    this.apachePOIExcel.writeOnExcelFile(packageFolder.getName(), getCrashCounter(packageFolder));
                }
                if (IS_SAPIENZ) {
                    int crashCounter = 0;
                    this.filters.add(packageFolder.getName());
                    for (File _apk : packageFolder.listFiles()) {
                        String sapienzCrashLogFolderPath = _apk.getAbsolutePath() + '/' + _apk.getName().split("\\.apk")[0] + ".apk_output/crashes";
                        crashCounter+= commandExecutor.countSapienzCrashLogs(sapienzCrashLogFolderPath);
                    }
                    this.apachePOIExcel.writeOnExcelFile(packageFolder.getName(), crashCounter);
                }
            }

        } else {
            logger.log(Level.WARNING, ALL_CRASHES_DIR.getAbsolutePath() + " does not exist!");
        }
    }

    /**
     * Excel instance gets created
     */
    @Override
    public void createExcelInstance() {
        this.apachePOIExcel = new ApachePOIExcel(Integer.toString(this.currentTestGenerationIndex));
    }

    /**
     * Title for the Excel file is set
     */
    @Override
    public void setExcelSheetTitle() {
        String sheetTitle = "Experiment Range: From TestGeneration_" + ExperimentLauncher.getFirstExperimentIndex() + " to TestGeneration_" + ExperimentLauncher.getLastExperimentIndex();
        this.apachePOIExcel.writeTitle(sheetTitle);
    }

    /**
     * Writes the outputStream (containing the data of the current_excel_file) into the "OnGoing" Excel File
     *
     * @param outputStream
     * @throws IOException
     */
    @Override
    public void writeResultsOnExcelSheet(FileOutputStream outputStream) throws IOException {
        this.apachePOIExcel.write(outputStream);
    }

    /**
     * It iterates through the parent directory of "ALL_CRASHES_DIR" and writes
     * into the excel file all the apps which don't have collected a crash inside their folder.
     *
     * @param ALL_CRASHES_DIR (A Folder in the current TestGeneration Folder, which contains all the crash_log files found during the testing)
     */
    private void computeAppsWithNoCrashes(final File ALL_CRASHES_DIR) {
        File testGenerationFolder = new File(ALL_CRASHES_DIR.getParent());
        if (testGenerationFolder.exists()) {
            ArrayList<File> folders = new ArrayList<>(Arrays.asList(testGenerationFolder.listFiles()));
            for (File folder : folders) {
                if (folder.getName().contains("ALL_CRASHES") || !folder.isDirectory()) {
                    continue;
                }
                if (!this.filters.contains(folder.getName())) {
                    if (IS_MONKEY) {
                        this.apachePOIExcel.writeOnExcelFile(folder.getName(), 0);
                    }
                    if (IS_SAPIENZ) {
                        this.apachePOIExcel.writeOnExcelFile(folder.getName().split("_")[0], 0);
                    }
                }
            }
        }
    }


    /**
     * It goes through each crash_log file and for each file calculates how many crashes there are inside it. It sums
     * the results and returns the corresponding integer number
     *
     * @param _package (_package File, which is the directory where the crash logs are stored)
     * @return number of crash for the given _package
     */
    private int getCrashCounter(File _package) {
        int numberOfCrashes = 0;
        for (File logs : _package.listFiles()) {
            if (logs.getName().startsWith("crash_log")) {
                numberOfCrashes += commandExecutor.countNrOfMonkeyCrashInsideACrashLog(logs.getAbsolutePath());
            }
        }
        return numberOfCrashes;
    }


    /**
     * Returns the current Index of the TestGeneration Folder (last folder that has been created)
     *
     * @param reportDir (directory where are stored the reports; either MonkeyReports or SapienzReporst).
     *                  It is defines by default in the constructor
     * @return (a integer number which is the current TestGeneration Folder index)
     */
    private int getCurrentTestGenerationFolderIndex(File reportDir) {
        ArrayList<Integer> indexes = new ArrayList<>();
        ArrayList<File> files = new ArrayList<>(Arrays.asList(reportDir.listFiles()));
        for (File file : files) {
            if (file.getName().contains("TestGeneration")) {
                indexes.add(Integer.valueOf(file.getName().split("_")[1]));
            }
        }
        if (indexes.isEmpty())
            return 0;
        else
            return Collections.max(indexes);
    }
}
