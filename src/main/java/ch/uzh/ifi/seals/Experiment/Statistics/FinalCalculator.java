package ch.uzh.ifi.seals.Experiment.Statistics;

import ch.uzh.ifi.seals.Experiment.ExcelFileGenerator.ApachePOIExcel;
import ch.uzh.ifi.seals.Experiment.ExcelFileGenerator.ExcelInstance;
import ch.uzh.ifi.seals.Experiment.ExperimentLauncher;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.costants.Enumerations.Extensions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import static ch.uzh.ifi.seals.Main.Main.REPORT_DIRECTORY;

/**
 * The FinalCalculator class computes and writes the "Final" statistics for the whole experiment.
 * It goes through each "On-going" Excel file and summarizes and writes the results into a unique excel file.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 03.04.2017
 */
public class FinalCalculator implements ExcelInstance {

    private ApachePOIExcel apachePOIExcel;
    private int firstIndex;
    private int lastIndex;
    private HashMap<String, Integer> finalStatistics;

    public FinalCalculator() throws InterruptedException, IOException {
        setEnvironment();
    }

    /**
     * It sets the environment for this class.
     * This method is invoked each time a new object of this class is created and instantiated.
     *
     * @throws InterruptedException
     */
    private void setEnvironment() throws InterruptedException, IOException {
        ConfigurationManager config = ConfigurationManager.getInstance();
        this.firstIndex = ExperimentLauncher.getFirstExperimentIndex();
        this.lastIndex = ExperimentLauncher.getLastExperimentIndex();
        this.finalStatistics = new HashMap<>();
    }

    /**
     * computes the statistics for the whole experiment. It generates an excel file which summarizes all the
     * "On-Going"-excel final. This method / class is invoked at the end of the experiment
     *
     * @throws IOException
     */
    public void computeExperimentStatistics() throws IOException {
        HashMap<String, Integer> values;
        final File EXPERIMENT_EXCEL_FILE = new File(REPORT_DIRECTORY + "/Experiment_" + this.firstIndex + "_" + this.lastIndex + "/Experiment_" + this.firstIndex + "_" + this.lastIndex + Extensions.EXCEL.prefix());
        final FileOutputStream outputStream = new FileOutputStream(EXPERIMENT_EXCEL_FILE);

        createExcelInstance();

        boolean isFirstTime = true;
        for (int excelFileIndex = this.firstIndex; excelFileIndex <= this.lastIndex; excelFileIndex++) {
            String excelFullPath = REPORT_DIRECTORY + "/TestGeneration_" + excelFileIndex + "/TestGeneration_" + excelFileIndex + ".xlsx";
            values = getOnGoingStatistics(excelFileIndex, new File(excelFullPath));
            updateExperimentExcelFile(values, isFirstTime);
            isFirstTime = false;
        }
        setExcelSheetTitle();
        writeResultsOnExcelSheet(outputStream);
    }

    /**
     * The Apache-POI-Excel Instance is created. The constructor's parameters represent the name of the sheet (above)
     */
    @Override
    public void createExcelInstance() {
        this.apachePOIExcel = new ApachePOIExcel(this.firstIndex + "_" + this.lastIndex);
    }

    /**
     * writes the resulting HashMap (the summary of the all "On-Going" Excel files) into the Experiment Excel File
     * Key: Package_Name, value: number of crashes
     *
     * @param outputStream
     * @throws IOException
     */
    @Override
    public void writeResultsOnExcelSheet(FileOutputStream outputStream) throws IOException {
        for (String _package : this.finalStatistics.keySet()) {
            this.apachePOIExcel.writeOnExcelFile(_package, this.finalStatistics.get(_package));
        }
        this.apachePOIExcel.write(outputStream);
        this.apachePOIExcel.close();
    }

    /**
     * sets the title for the Experiment Excel File
     */
    public void setExcelSheetTitle() {
        String sheetTitle = "Experiment Range: From TestGeneration_" + this.firstIndex + " to TestGeneration_" + this.lastIndex;
        this.apachePOIExcel.writeTitle(sheetTitle);
    }


    /**
     * Updates the HashMap @finalStatistics, which contains the data for Experiment Excel file.
     * It searches each key (packageName) and updates its value (number of crashes)
     *
     * @param values      (Current HashMap containing the keys & values of the current "On-Going" Excel File)
     * @param isFirstTime (boolean value for writing the package names only once into the Experiment Excel File)
     * @return
     */
    private void updateExperimentExcelFile(HashMap<String, Integer> values, boolean isFirstTime) {
        for (String _packageName : values.keySet()) {
            if (_packageName.isEmpty()) {
                continue;
            }
            if (isFirstTime) {
                this.finalStatistics.put(_packageName, values.get(_packageName));
            } else {
                this.finalStatistics.put(_packageName, this.finalStatistics.get(_packageName) + values.get(_packageName));
            }
        }
    }

    /**
     * Returns a hashMap containing the packageName stored in the given ExcelFile (as key),
     * associated with its number of crashes (value)
     *
     * @param excelFile
     * @return
     * @throws IOException
     */
    private HashMap<String, Integer> getOnGoingStatistics(int excelFileIndex, File excelFile) throws IOException {
        FileInputStream inputStream = new FileInputStream(excelFile);
        return this.apachePOIExcel.getOnGoingStatistics(inputStream);
    }
}
