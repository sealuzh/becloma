package ch.uzh.ifi.seals.Parsers;

import ch.uzh.ifi.seals.config.ClusteringManager;
import ch.uzh.ifi.seals.config.ConfigurationManager;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by LuckyP on 31.05.17.
 */
public abstract class PreProcessedReportParser {
    private static final String DELIMITER = ",";
    private static final String DATASET_DIRECTORY_PATH = ClusteringManager.getInstance().getPreProcessedStackTracesDirectory();

    private static Stream<String[]> getStream() throws IOException {
        return Files.lines(Paths.get(DATASET_DIRECTORY_PATH))
                .map(line -> line.split(DELIMITER));
    }

    public static HashMap<String, String> getClassesFromCSV() throws IOException {
        HashMap<String, String> collection = new HashMap<>();
        getStream().forEach(split -> {
            String pClass = split[0].replace("\"", "");
            String[] classSplit = pClass.split("\\.");
            pClass = classSplit[classSplit.length - 1];
            String additionalWords = split[1];
            collection.put(pClass, additionalWords);
        });
        return collection;
    }


}
