package ch.uzh.ifi.seals.Parsers;

import ch.uzh.ifi.seals.config.ConfigurationManager;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static ch.uzh.ifi.seals.Main.Main.IS_DEVICE;
import static ch.uzh.ifi.seals.Main.Main.IS_EMULATOR;

/**
 * This class parses and updates the Sapienz setting file located in the Sapienz directory under
 * the name of <settings.py>
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 03.02.2017
 */
public abstract class SapienzSettingsParser {

    /**
     * This method parses and updates the Setting file of Sapienz
     *
     * @throws IOException
     */
    public static void updateSettingSapienz() throws IOException {
        Path file = Paths.get(ConfigurationManager.getInstance().getSapienzWorkDirectory() + "/settings_model.py");
        Path writeToFile = file.resolveSibling("settings.py");
        if (!Files.exists(file)) {
            throw new FileNotFoundException(file.toAbsolutePath().toString());
        }
        String isDevice = "";
        String isEmulator = "";
        if (IS_DEVICE) {
            isDevice = "True";
            isEmulator = "False";
        } else if (IS_EMULATOR) {
            isDevice = "False";
            isEmulator = "True";
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(writeToFile.toFile()));
        Stream<String> stream = Files.lines(file);
        String finalIsDevice = isDevice;
        String finalIsEmulator = isEmulator;
        stream.forEach(line -> {
            String[] temp = line.split("=");
            // TODO cambiare automaticamente i settings per sapienz
            if (line.startsWith("IS_DEVICE")) {
                line = line.replace(temp[1].trim(), finalIsDevice);
            }

            if (line.startsWith("IS_EMULATOR")) {
                line = line.replace(temp[1].trim(), finalIsEmulator);
            }

            if (line.startsWith("AVD_BOOT_DELAY")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getAVDBootDelay());
            }

            if (line.startsWith("AVD_SERIES")) {
                line = line.replace(temp[1].trim(), "\"" + ConfigurationManager.getInstance().getEmulatorName() + "\"");
            }

            if (line.startsWith("ANDROID_HOME")) {
                String[] splitSlash = ConfigurationManager.getInstance().getAdbExecDirectory().split("sdk");
                String trim = (splitSlash[0] + "sdk/").trim();
                line = line.replace(temp[1].trim(), "'" + trim + "'");
            }

            if (line.startsWith("WORKING_DIR")) {
                line = line.replace(temp[1].trim(), "'" + ConfigurationManager.getInstance().getSapienzWorkDirectory() + "/'");
            }

            if (line.startsWith("TOOL_DIR")) {
                line = line.replace(temp[1].trim(), "'" + ConfigurationManager.getInstance().getToolDirectory() + "'");
            }

            if (line.startsWith("SEQUENCE_LENGTH_MIN")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getSequenceLengthMin());
            }

            if (line.startsWith("SEQUENCE_LENGTH_MAX")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getSequenceLengthMax());
            }

            if (line.startsWith("SUITE_SIZE")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getSuiteSize());
            }

            if (line.startsWith("POPULATION_SIZE")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getPopulationSize());
            }

            if (line.startsWith("OFFSPRING_SIZE")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getOffspringSize());
            }

            if (line.startsWith("GENERATION")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getGeneration());
            }

            if (line.startsWith("CXPB")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getCrossoverProbability());
            }

            if (line.startsWith("MUTPB")) {
                line = line.replace(temp[1].trim(), ConfigurationManager.getInstance().getMutationProbability());
            }
            try {
                writer.write(line + "\n");
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


}
