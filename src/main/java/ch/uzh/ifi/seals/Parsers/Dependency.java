package ch.uzh.ifi.seals.Parsers;

/**
 * This Class Dependency defines the structure of a dependency object. A dependency represents all the libraries
 * used by a programmer for developing his application.
 *
 * @author Giovanni Grano
 * @version 1.0
 * @since 25.01.2017
 */
public class Dependency {

    private String group;
    private String name;
    private String version;

    public Dependency(String group, String name, String version) {
        this.group = group;
        this.name = name;
        this.version = version;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Dependency{" +
                "group='" + group + '\'' +
                ", name='" + name + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
