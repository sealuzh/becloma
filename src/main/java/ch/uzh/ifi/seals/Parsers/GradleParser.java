package ch.uzh.ifi.seals.Parsers;

import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.Tester.CommandExecutor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by Lucas Pelloni on 24.12.16.
 * find /Users/LuckyP/Desktop/UZH/BA/Download/SRCS -type f -name "*build.gradle" >> /Users/LuckyP/Desktop/TEST.txt
 */

public class GradleParser {

    CommandExecutor com = new CommandExecutor();

    private ConfigurationManager config;

    public GradleParser() {
        this.config = ConfigurationManager.getInstance();
    }

    public ArrayList<String> getAllBuildGradlePath() {
        return com.toReadArrayList(this.config.getGradleFilesDirectory());
    }


    public ArrayList<String> parseBuildGradle(String path) {
        ArrayList<String> dependencies = new ArrayList<>();
        File file = new File(path);
        String split[] = path.substring(43).split("_");
        String packageName = split[0];
        dependencies.add("Path: " + path);
        dependencies.add("Package name: " + packageName + "\n");
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            BufferedReader tempReader;
            String line;
            String temp;
            int counterDep = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains("dependencies") && line.contains("{")) {
                    tempReader = bufferedReader;
                    // write "dependencies"
                    boolean isFirstDep = true;
                    while (!((temp = tempReader.readLine()).contains("}"))) {
                        if (isFirstDep) {
                            dependencies.add(++counterDep + ". dependencies {");
                            isFirstDep = !isFirstDep;
                        }
                        dependencies.add("\t" + temp.trim());
                    }
                    // dep finish
                    dependencies.add("}" + "\n");

                }
            }

            dependencies.add("--------------------------------------");
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dependencies;
    }

    /**
     * TODO extend the parsing to other kind of dependency (default method instead contains())
     */
    public List<Dependency> getDependenciesFromGradleFile(String file) {
        List<Dependency> depList = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            ArrayList<String> dependencies = new ArrayList<>(Arrays.asList(
                    stream.filter(x -> x.contains("classpath")).toArray(String[]::new)));
            dependencies.forEach(x -> depList.add(extractDependency(x)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return depList;
    }

    public Dependency extractDependency(String stringToParse) {
        Dependency dependency = null;
        Pattern pattern = Pattern.compile("'(.*?)'");
        Matcher matcher = pattern.matcher(stringToParse.trim());
        String entireDep = null;
        if (matcher.find()) {
            entireDep = matcher.group(1);
        }
        if (entireDep != null) {
            String[] split = entireDep.split(":");
            dependency = new Dependency(split[0], split[1], split[2]);
        }
        return dependency;
    }

    public void parseAllBuildGradle() {
        CommandExecutor com = new CommandExecutor();
        ArrayList<String> allGradlePath = getAllBuildGradlePath();
        ArrayList<ArrayList<String>> allDependencies = new ArrayList<>();
        for (String gradlePath : allGradlePath) {
            allDependencies.add(parseBuildGradle(gradlePath));
        }

        // write into a file all Dependencies
        for (ArrayList<String> dependency : allDependencies) {
            com.printDataSet(dependency);
        }
    }

    public void writeDependecies() {
        ArrayList<String> files = null;
        try (Stream<String> stream = Files.lines(Paths.get(this.config.getGradleFilesDirectory()))) {
            files = new ArrayList<>(Arrays.asList(
                    stream.toArray(String[]::new)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GradleParser p = new GradleParser();
        List<Dependency> deps = new ArrayList<>();
        for (String f : files) {
            System.out.println(f);
            deps.addAll(p.getDependenciesFromGradleFile(f));
        }
        deps.forEach(System.out::println);
    }


}
