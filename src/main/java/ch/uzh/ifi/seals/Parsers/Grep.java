package ch.uzh.ifi.seals.Parsers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * The Grep class implements a kind of Grep command (similar to the one implemented for the terminal).
 * It scans the LOG files and builds a customized crash log if there is any.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 20.02.2017
 */
public class Grep {

    private static final String START_CRASH = "// CRASH";
    private static final String END_CRASH = "//";
    private static final String FIRST_ROWS_REGEX = "//\\s\\s\\s\\w+:|//\\sEvent|//\\sMonkey";
    private static final Pattern START_PATTERN = Pattern.compile(START_CRASH);
    private static final Pattern END_PATTERN = Pattern.compile(END_CRASH);
    private String LOG_PATH;

    public Grep(final String LOG_PATH) throws IOException {
        this.LOG_PATH = LOG_PATH;
    }

    private Stream<String> getStream() throws IOException {
        return Files.lines(Paths.get(LOG_PATH))
                .filter(line -> !Pattern.compile(FIRST_ROWS_REGEX).matcher(line).find());
    }

    /**
     * This method counts how many crashes an application has
     *
     * @return the number of pattern occurrences
     */
    public int getNumberOfCrashes() throws IOException {
        final int[] crashCounter = {0};
        getStream().forEach(line -> {
            Matcher m = START_PATTERN.matcher(line);
            if (m.find())
                crashCounter[0]++;
        });
        return crashCounter[0];
    }

    /**
     * It parses the whole log created by monkey during the test of an application and builds the crash
     * log if there is any.
     *
     * @return
     * @throws IOException
     */
    public ArrayList<String> getCrashLog() throws IOException {
        final int[] crashCounter = {0};
        ArrayList<String> crashLog = new ArrayList<>();
        getStream().forEach(line -> {
            Matcher m = START_PATTERN.matcher(line);
            if (m.find()) {
                if (crashCounter[0] == 0) {
                    crashLog.add("Crash number: " + Integer.toString(++crashCounter[0]));
                } else {
                    crashLog.add("\n\nCrash number: " + Integer.toString(++crashCounter[0]));
                }
            }
            if (line.startsWith("// ")) {
                crashLog.add(line);
            }
        });
        return crashLog;
    }
}