package ch.uzh.ifi.seals.Installer;

import java.io.IOException;

/**
 * The Class Multinstaller install on the device/emulator the given apk simultaneously
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 14.04.2017
 */
public class MultiInstaller implements Runnable {

    private Thread thread;
    private String deviceName;
    private String apkName;

    public MultiInstaller(String apkName, String deviceName) {
        this.apkName = apkName;
        this.deviceName = deviceName;
    }

    @Override
    public void run() {
        try {
            Installer.installApp(apkName, deviceName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Thread start() {
        System.out.println("Installing " + apkName + " on " + deviceName + "...");
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
        return this.thread;
    }


}
