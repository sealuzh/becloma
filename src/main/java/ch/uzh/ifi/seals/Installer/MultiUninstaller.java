package ch.uzh.ifi.seals.Installer;

import java.io.IOException;

/**
 * Created by LuckyP on 14.04.17.
 */
public class MultiUninstaller implements Runnable {
    private Thread thread;
    private String packageName;
    private String deviceName;

    public MultiUninstaller(String packageName, String deviceName) {
        this.packageName = packageName;
        this.deviceName = deviceName;
    }

    @Override
    public void run() {
        try {
            Installer.uninstallApp(packageName, deviceName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Thread start() {
        System.out.println("Uninstalling " + packageName + " on " + deviceName + "...");
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
        return this.thread;
    }
}
