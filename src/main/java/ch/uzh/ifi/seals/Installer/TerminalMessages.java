package ch.uzh.ifi.seals.Installer;

/**
 * The Enumeration TerminalMessages contains some of the most common terminal messages that can appear during
 * the (un)installation of one application on an a Android device.
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 05.04.2017
 */
public enum TerminalMessages {

    /**
     * Install errors
     */
    INSTALL_FAILED_ALREADY_EXISTS("INSTALL_FAILED_ALREADY_EXISTS"),// app already installed
    INSTALL_FAILED_VERSION_DOWNGRADE("INSTALL_FAILED_VERSION_DOWNGRADE"), //you're trying to install an app with the same packageName as an app that's already installed on the emulator, but the one you're trying to install has a lower versionCode (integer value for your version number).
    INSTALL_FAILED_NO_MATCHING_ABIS("INSTALL_FAILED_NO_MATCHING_ABIS"), // Failed to extract native libraries


    /**
     * Uninstall errors
     */
    DELETE_FAILED_INTERNAL_ERROR("DELETE_FAILED_INTERNAL_ERROR"), // internal error, delete app manually
    UNKNOWN_PACKAGE("IllegalArgumentException"), // unknown package

    /**
     * Uninstall/Install Success
     */
    SUCCESS("Success");

    private String message;

    TerminalMessages(String message) {
        this.message = message;
    }

    public String getMsg() {
        return this.message
                ;
    }

}
