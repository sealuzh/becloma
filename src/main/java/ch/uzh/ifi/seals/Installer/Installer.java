package ch.uzh.ifi.seals.Installer;

import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import ch.uzh.ifi.seals.costants.CommandLines;
import ch.uzh.ifi.seals.costants.Enumerations.Extensions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Class Installer implements all these methods, which have somethings to do with
 * the installation of the applications on a emulator/device
 *
 * @author Lucas Pelloni
 * @version 1.0
 * @since 10.02.2017
 */
public class Installer {

    // APK Directory, which contains all the apks that have to be installed
    private static final String APK_DIRECTORY_PATH = ConfigurationManager.getInstance().getSaveApkDirectory();
    private static final File[] APK_DIRECTORY_FILES = new File(APK_DIRECTORY_PATH).listFiles();
    private static Logger logger;
    private static ArrayList<TerminalMessages> terminalMessages;

    public Installer() {
        logger = Logger.getLogger(Installer.class.getName());
        terminalMessages = new ArrayList<>();
        terminalMessages.add(TerminalMessages.INSTALL_FAILED_ALREADY_EXISTS);
        terminalMessages.add(TerminalMessages.DELETE_FAILED_INTERNAL_ERROR);
        terminalMessages.add(TerminalMessages.UNKNOWN_PACKAGE);
        terminalMessages.add(TerminalMessages.INSTALL_FAILED_NO_MATCHING_ABIS);
        terminalMessages.add(TerminalMessages.INSTALL_FAILED_VERSION_DOWNGRADE);
        terminalMessages.add(TerminalMessages.SUCCESS);
    }


    /**
     * Validates the given process. If the process returns the desired value "Success" is printed to the console,
     * otherwise "False"
     *
     * @param APK_NAME (name of the given APK file which has to be installed
     * @param p        (Process which has to be validated)
     * @throws IOException
     */
    private static void verifyProcessCompletion(final String APK_NAME, Process p) throws Exception {
        if (isSuccess(p)) {
            System.out.println(APK_NAME + ": Success");
        } else {
            System.err.println(APK_NAME + ": Failure");
        }
    }

    /**
     * Returns a boolean value. It examines the output written to the terminal when the given
     * process p is triggered. If the return value is false, means that the process met some errors during
     * its execution.
     *
     * @return (true or false)
     */
    private static boolean isSuccess(Process p) throws IOException, InterruptedException {
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String lineIterator;
        while ((lineIterator = errorReader.readLine()) != null) {
            stringBuilder.append(lineIterator).append("\n");
            System.out.println(lineIterator);
        }
        while ((lineIterator = reader.readLine()) != null) {
            stringBuilder.append(lineIterator).append("\n");
            System.out.println(lineIterator);
        }
        Thread.sleep(1500L);
        final String TERMINAL_OUTPUT = stringBuilder.toString();
        for (TerminalMessages message : terminalMessages) {
            if (TERMINAL_OUTPUT.contains(message.getMsg())) {
                return !hasError(message);
            }
        }
        logger.log(Level.SEVERE, "Unknown error: " + TERMINAL_OUTPUT);
        return false;
    }

    /**
     * It checks whether the terminal output contains some of the most common messages described in the enumeration TerminalMessages
     *
     * @param message
     * @return (true or false)
     */
    private static Boolean hasError(TerminalMessages message) {
        switch (message) {
            case UNKNOWN_PACKAGE:
                logger.log(Level.INFO, message.getMsg() + ": You are trying to uninstall a package which is not installed on the emulator / device.");
                return false;
            case DELETE_FAILED_INTERNAL_ERROR:
                logger.log(Level.SEVERE, message.getMsg() + ": An internal error occurred, please delete the app manually");
                return true;
            case INSTALL_FAILED_ALREADY_EXISTS:
                logger.log(Level.INFO, message.getMsg() + ": The package is already installed!");
                return false;
            case INSTALL_FAILED_NO_MATCHING_ABIS:
                logger.log(Level.SEVERE, message.getMsg() + ": Failed to extract native libraries, please delete the app manually");
                return true;
            case INSTALL_FAILED_VERSION_DOWNGRADE:
                logger.log(Level.SEVERE, message.getMsg() + ": You're trying to install an app with the same packageName as an app that's already installed on the emulator, but the one you're trying to install has a lower versionCode");
                logger.log(Level.INFO, "Check possible solutions here: http://stackoverflow.com/questions/13808599/android-emulator-installation-error-install-failed-version-downgrade");
                return true;
            case SUCCESS:
                return false;
        }
        return true;
    }

    /**
     * Installs on the emulator / device the given application
     *
     * @param APK_NAME (The name of the app which has to be installed)
     * @throws IOException
     * @throws InterruptedException
     * @paramFormat APK_file: for example "org.telegram.messenger_8513.apk"
     */
    public static void installApp(final String APK_NAME, final String DEVICE_NAME) throws Exception {
//        System.out.println("Installing " + APK_NAME + "...");
        final String COMMAND = CommandLines.getInstallApp(DEVICE_NAME) + APK_NAME;
        Process p = CommandExecutor.getProcess(COMMAND);
        verifyProcessCompletion(APK_NAME, p);
    }

    /**
     * Uninstalls from the emulator / device the given application
     *
     * @param PACKAGE_NAME (The name of the app which has to be uninstalled)
     * @throws IOException
     * @throws InterruptedException
     * @paramFormat package_name: for example "org.telegram.messenger"
     */
    public static void uninstallApp(final String PACKAGE_NAME, final String DEVICE_NAME) throws Exception {
//        System.out.println("Uninstalling " + PACKAGE_NAME + "...");
        final String COMMAND = CommandLines.getUninstallApp(DEVICE_NAME) + PACKAGE_NAME;
        Process p = CommandExecutor.getProcess(COMMAND);
        verifyProcessCompletion(PACKAGE_NAME, p);
    }

    /**
     * It goes through the APK_DIRECTORY_PATH and installs all the applications contained inside it
     *
     * @throws IOException
     * @throws InterruptedException
     * @paramFormat PATH represented as String
     */
    public void installAllApp(final String DEVICE_NAME) throws Exception {
        assert APK_DIRECTORY_FILES != null;
        for (File apk : APK_DIRECTORY_FILES) {
            if (apk.getName().endsWith(Extensions.ANDROID_APP.prefix())) {
                System.out.println("Installing " + apk);
                TimeUnit.SECONDS.sleep(2L);
                installApp(apk.getName(), DEVICE_NAME);
            }

        }
    }

    /**
     * It goes through the APK_DIRECTORY_PATH and uninstalls all the applications contained inside it
     *
     * @throws IOException
     * @throws InterruptedException
     * @paramFormat PATH represented as String
     */
    public void uninstallAllApp(final String DEVICE_NAME) throws Exception {
        assert APK_DIRECTORY_FILES != null;
        for (File apk : APK_DIRECTORY_FILES) {
            if (apk.getName().endsWith(Extensions.ANDROID_APP.prefix())) {
                System.out.println("Uninstalling " + apk + " counter: ");
                TimeUnit.SECONDS.sleep(2L);
                final String PACKAGE_NAME = apk.getName().split("_")[0];
                uninstallApp(PACKAGE_NAME, DEVICE_NAME);
            }

        }
    }
}
