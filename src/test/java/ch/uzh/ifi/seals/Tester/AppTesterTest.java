package ch.uzh.ifi.seals.Tester;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by LuckyP on 16.03.17.
 */
class AppTesterTest {

    @Test
    void testAllApp() {


    }

    @Test
    void generateMonkeyCrashLog() throws InterruptedException, IOException {
        AppTester appTester = new AppTester();
        String packagePath = "src/test/Reports/MonkeyReports/apps.babycaretimer";
        String crashFile = "/reportWithCrash.txt";
        String noCrashFile = "/reportWithoutCrash.txt";
        boolean hasCrashLog = false;

        File packageFolder = new File(packagePath);
        ArrayList<File> files = new ArrayList<>(Arrays.asList(packageFolder.listFiles()));
//        appTester.generateMonkeyCrashLog(packagePath, crashFile,0);

        // after generating crash log
        for (File file : files) {
            if (file.getName().contains("crash_log_1.txt")) {
                hasCrashLog = true;
            }
        }

        assertTrue(hasCrashLog);
    }

}