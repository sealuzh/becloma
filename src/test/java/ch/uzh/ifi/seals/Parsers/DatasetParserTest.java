package ch.uzh.ifi.seals.Parsers;

import ch.uzh.ifi.seals.FDroid_Repo.FDroidRepoFilter;
import ch.uzh.ifi.seals.Tester.CommandExecutor;
import ch.uzh.ifi.seals.config.ConfigurationManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by LuckyP on 16.03.17.
 */
class DatasetParserTest {

    @Test
    void getPackagesWithDatesFromCSV() throws IOException {
        ConfigurationManager c = ConfigurationManager.getInstance();
        HashMap<String, ArrayList<Date>> map = DatasetParser.getPackagesWithDatesFromCSV();

        for (String key : map.keySet()) {
            ArrayList<Date> dates = map.get(key);
            // each arraylist of dates contains an initial date and a final date
            assertTrue(dates.size() == 2);
            Date initialDate = dates.get(0);
            Date finalDate = dates.get(1);

            // asserts that the final date comes after the initial date
            assertTrue(finalDate.after(initialDate));

//          0 if the argument Date is equal to this Date; a value less than 0
//          if this Date is before the Date argument; and a value greater than
//          0 if this Date is after the Date argument.
            assertTrue(initialDate.compareTo(finalDate) < 0);
        }
    }

    @Test
    void getPackagesFromCSV() throws IOException {
        ConfigurationManager config = ConfigurationManager.getInstance();
        CommandExecutor c = new CommandExecutor();

//        c.printDataSet(d.getPackagesFromCSV(config.getCSVDirectory()));
        System.out.println(DatasetParser.getPackagesFromCSV().size());

        FDroidRepoFilter fDroidRepoFilter = new FDroidRepoFilter();
//        c.printDataSet(fDroidRepoFilter.getFilteredFDroidRepo(d.getPackagesFromCSV(config.getCSVDirectory())));
        System.out.println(fDroidRepoFilter.getLinksLastAPKVersion(DatasetParser.getPackagesFromCSV()).size());

        for (String i : DatasetParser.getPackagesFromCSV()) {
            for (String y : fDroidRepoFilter.getLinksLastAPKVersion(DatasetParser.getPackagesFromCSV())) {
                if (y.contains(i)) {
                    System.err.println(i);
                }
            }
        }
    }

}