package ch.uzh.ifi.seals.Parsers;

import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by LuckyP on 20.03.17.
 */
class SAXParserXMLTest {

    @Test
    void getLastAPKsVersionFromXML() {
        SAXParserXML parser = new SAXParserXML();
        assertTrue(!parser.getAPKSLinksFromXMLFile().isEmpty());
    }

    @Test
    void getAPKSWithDateFromXMLFile() {
        SAXParserXML saxParserXML = new SAXParserXML();
        HashMap<String, Date> test = saxParserXML.getAPKSWithDateFromXMLFile();

        for (String _package: test.keySet()){
            System.out.println("Package: "+_package+"     date: "+ test.get(_package));
        }
    }

}