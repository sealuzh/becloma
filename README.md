# Testing Android Applications 
### General Configuration
* Brew installed _(if not installed yet)_ 
* Android SDK platform-tool installed _(if not installed yet)_
* pip installed _(if not installed yet)_

Install Brew with the Command Line (OSX): 

    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    
Install Android on OSX:
   
   * Just download <a href="https://developer.android.com/studio/index.html?hl=sk#tos-header">Android Studio</a> and install it
    
Install pip with the Command Line (OSX):

    $ brew install pip
    or if it fails
    $ sudo easy_install pip
    
### Sapienz Environment Configration
* Android SDK:
    Install an emulator/device, which has the **API 19** level (Android 4.4: KitKat)

* Mac OS:
    
        $ brew install coreutils for gtimeout
    
* Environment Variables: 

    Include path for `aapt` command: ```/Users/<user_name>/Library/Android/sdk/build-tools/19.x.x```
    
     _Find out your API version:_
     
        $ cd /Users/<user_name>/Library/Android/sdk/build-tools
        $ ls
        
    _Open your bash_profile:_
    
        $ cd ~
        $ nano .bash_profile
        
    _Copy the following command inside it and save it_
    
        export PATH=/Users/<user_name>/Library/Android/sdk/build-tools/19.x.x:$PATH
        $ source .bash_profile
        
* Install Python dependencies (inside the sapienz directory):

        $ sudo pip install -r requirements.txt
        
* Python: 2.7

    There could be a problem with the installation of the Python dependencies. If you are not able to install all the 
    dependencies, run the following command line again:
    
        $ brew install python 


### Monkey Environment Configuration
* Android SDK installed


### Configuration File  
In order to start your testing section correctly, you have to update the **config.properties** file: 

1. Choose the testing mode you want to perform: set either IS_EMULATOR or IS_DEVICE **True** resp. **False**

    NB: If you want to perform your tests on a real device, be sure that the _Developer Mode_ has been activated
2. Choose the testing tool you want to use. Set either IS_SAPIENZ or IS_MONKEY **True** resp. **False**

    NB: If you use Sapienz, be sure that **ALL** the requisites have already been installed/configurated
    
3. Set your ANDROID SDK directory here: _adb_exec_directory_

4. Set your PROJECT directory here: _sapienz_tool_directory_

5. Set your _emulator_name_ or _device_name_

6. Set the number of iterations you want to perform (how many times the whole _data_set_ resp. your _apks_ will be tested)

7. Set your MONKEY resp. SAPIENZ parameters

### Outputs information
* All output files are saved into the _Reports/MonkeyReports/TestGeneration_Nr_ resp. _Reports/SapienzReports/TestGeneration_Nr_
* At the end of the experiment, the generated crash logs are automatically copied into the ALL_CRASHES folder for each Test Generation




